# This migration comes from spree_customized (originally 20141230034318)
class AddPages < ActiveRecord::Migration
  def change
    create_table :spree_pages do |t|
      t.string  :title
      t.text    :content
      t.string  :slug
      t.integer :tenant_id
    end
    add_index :spree_pages, :tenant_id
  end
end
