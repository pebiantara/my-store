class AddTabelActivityLogs < ActiveRecord::Migration
  def change
    create_table :spree_activity_logs do |t|
      t.integer :source_id
      t.string :source_type
      t.string :activity
      t.integer :edit_by
      t.string  :old_attributes
      t.string  :ip_address
      t.datetime :login_at
      t.timestamps
    end
  end
end
