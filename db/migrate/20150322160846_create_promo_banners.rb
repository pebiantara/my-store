class CreatePromoBanners < ActiveRecord::Migration
  def change
    create_table :spree_promo_banners do |t|
      t.string     :type_banner
      t.string     :url
      t.integer    :product_id
      t.integer    :tenant_id
      t.timestamps
    end
  end
end
