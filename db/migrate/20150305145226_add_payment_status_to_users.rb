class AddPaymentStatusToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :payment_status, :string, default: 'open'
  end
end
