class ChangeOldAttributesFromActivityLogs < ActiveRecord::Migration
  def change
    change_column :spree_activity_logs, :old_attributes, :text
  end
end
