class CreateSpreeSocial < ActiveRecord::Migration
  def change
    create_table :spree_socials do |t|
      t.string  :provider
      t.string  :url
      t.boolean :status
      t.integer :tenant_id 
      t.timestamps
    end

    add_column :spree_sliders, :link, :string
  end
end
