class AddFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :available_size, :string
    add_column :spree_products, :quantity_unit, :string
    add_column :spree_products, :product_flag, :string
    add_column :spree_products, :status, :boolean, default: true
  end
end
