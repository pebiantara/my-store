class AddPaymentTimeOutToSpreeBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :payment_time_out, :datetime
  end
end
