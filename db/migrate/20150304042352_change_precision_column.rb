class ChangePrecisionColumn < ActiveRecord::Migration
  def up
    change_column :spree_adjustments, :amount, :decimal, precision: 12, scale: 2
    change_column :spree_line_items, :price, :decimal, precision: 12, scale: 2
    change_column :spree_line_items, :cost_price, :decimal, precision: 12, scale: 2
    change_column :spree_orders, :item_total, :decimal, precision: 12, scale: 2
    change_column :spree_orders, :total, :decimal, precision: 12, scale: 2
    change_column :spree_orders, :adjustment_total, :decimal, precision: 12, scale: 2
    change_column :spree_orders, :payment_total, :decimal, precision: 12, scale: 2
    change_column :spree_payments, :amount, :decimal, precision: 12, scale: 2
    change_column :spree_return_authorizations, :amount, :decimal, precision: 12, scale: 2
    change_column :spree_shipments, :cost, :decimal, precision: 12, scale: 2
    change_column :spree_tax_rates, :amount, :decimal, precision: 12, scale: 2
    change_column :spree_variants, :cost_price, :decimal, precision: 12, scale: 2
    change_column :spree_prices, :amount, :decimal, precision: 12, scale: 2
    change_column :spree_donations, :total, :decimal, precision: 12, scale: 2
  end

  def down
    change_column :spree_adjustments, :amount, :decimal, precision: 8, scale: 2
    change_column :spree_line_items, :price, :decimal, precision: 8, scale: 2
    change_column :spree_line_items, :cost_price, :decimal, precision: 8, scale: 2
    change_column :spree_orders, :item_total, :decimal, precision: 8, scale: 2
    change_column :spree_orders, :total, :decimal, precision: 8, scale: 2
    change_column :spree_orders, :adjustment_total, :decimal, precision: 8, scale: 2
    change_column :spree_orders, :payment_total, :decimal, precision: 8, scale: 2
    change_column :spree_payments, :amount, :decimal, precision: 8, scale: 2
    change_column :spree_return_authorizations, :amount, :decimal, precision: 8, scale: 2
    change_column :spree_shipments, :cost, :decimal, precision: 8, scale: 2
    change_column :spree_tax_rates, :amount, :decimal, precision: 8, scale: 2
    change_column :spree_variants, :cost_price, :decimal, precision: 8, scale: 2
    change_column :spree_prices, :amount, :decimal, precision: 8, scale: 2
    change_column :spree_donations, :total, :decimal, precision: 8, scale: 2
  end
end
