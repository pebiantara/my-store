class AddTopProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :top_product, :boolean, :default => false
  end
end
