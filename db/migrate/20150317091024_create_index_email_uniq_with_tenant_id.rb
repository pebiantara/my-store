class CreateIndexEmailUniqWithTenantId < ActiveRecord::Migration
  def up
    remove_index "spree_users", :name => "email_idx_unique"
    add_index "spree_users", ["email", "tenant_id"], name: 'email_tenant_uniq', unique: true
  end

  def down
    add_index "spree_users", ["email"], :name => "email_idx_unique", :unique => true
    remove_index "spree_users", name: 'email_tenant_uniq'
  end
end
