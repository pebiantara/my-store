class AddTranslationsToPages < ActiveRecord::Migration
  def up
    params = { :title => :string, :content => :text, :slug => :string }
    Spree::Page.create_translation_table!(params, { :migrate_data => true })
  end
  
  def down
    Spree::Page.drop_translation_table! :migrate_data => true
  end
end