class AddColumnExpiredAtToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :expired_at, :datetime
  end
end
