# This migration comes from spree_customized (originally 20141224065247)
class AddSuperadminUsersAndMenuActive < ActiveRecord::Migration
  def change
    add_column :spree_users, :as_superadmin, :boolean, default: false
    add_column :spree_menus, :active, :boolean, default: true
  end
end
