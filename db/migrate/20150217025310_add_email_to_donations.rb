class AddEmailToDonations < ActiveRecord::Migration
  def change
    add_column :spree_donations, :email, :string
  end
end
