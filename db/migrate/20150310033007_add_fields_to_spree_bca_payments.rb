class AddFieldsToSpreeBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :transaction_date, :datetime
    add_column :spree_bca_payments, :currency, :string
    add_column :spree_bca_payments, :total_amount, :string
    add_column :spree_bca_payments, :auth_key, :string
  end
end
