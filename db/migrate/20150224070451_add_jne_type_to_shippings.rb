class AddJneTypeToShippings < ActiveRecord::Migration
  def change
    add_column :spree_jne_shippings, :jne_type, :string, default: 'reg'
  end
end
