class AddPaymentMethodIdAndPackageTypeToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :payment_method_id, :integer
    add_column :spree_users, :package_type, :string #patron, platinum, gold, silver, bronze, family
    add_column :spree_bca_payments, :member_id, :integer
  end
end
