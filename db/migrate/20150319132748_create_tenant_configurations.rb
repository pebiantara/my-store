class CreateTenantConfigurations < ActiveRecord::Migration
  def change
    create_table :spree_tenant_configurations do |t|
      t.datetime   :expired_at
      t.string     :type_registration #solar, premium, pertamax
      t.integer    :tenant_id
      t.string     :payment_status #paid, balance
      t.string     :status
      t.integer    :max_products
      t.integer    :max_orders
      t.integer    :max_products_image
      t.timestamps
    end
  end
end
