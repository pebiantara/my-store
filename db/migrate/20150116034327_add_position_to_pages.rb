class AddPositionToPages < ActiveRecord::Migration
  def change
    add_column :spree_pages, :position, :string, default: 'top'
  end
end
