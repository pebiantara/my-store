class CreateDonations < ActiveRecord::Migration
  def change
    create_table :spree_donations do |t|
      t.decimal :total, :precision => 8, :scale => 2
      t.integer :order_id
      t.integer :user_id
      t.string  :status, default: "open"
      t.integer :tenant_id
      t.timestamps
    end

    Spree::Menu.generate_data
  end
end
