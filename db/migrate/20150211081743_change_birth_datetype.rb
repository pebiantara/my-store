class ChangeBirthDatetype < ActiveRecord::Migration
  def change
    remove_column :spree_users, :birth_date, :integer
    add_column :spree_users, :birth_date, :date
    add_column :spree_users, :use_billing, :boolean, default: true
  end
end
