class AddUseJneAndJneCategoryToShippingMethods < ActiveRecord::Migration
  def change
    add_column :spree_shipping_methods, :use_jne, :boolean, default: false
    add_column :spree_shipping_methods, :jne_type, :string, default: 'reg' 
  end
end
