class AddExpiredTimeToBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :expired_at, :datetime
  end
end
