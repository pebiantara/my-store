class AddFieldsToFeatures < ActiveRecord::Migration
  def change
    add_column :spree_features, :controller_name, :string
    add_column :spree_features, :action_name, :string
  end
end
