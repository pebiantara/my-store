class AddIdentifierToDonations < ActiveRecord::Migration
  def change
    add_column :spree_donations, :identifier, :string
    add_column :spree_bca_payments, :donation_id, :integer
    add_column :spree_donations, :payment_method_id, :integer
    remove_column :spree_donations, :payment_method, :string
  end
end
