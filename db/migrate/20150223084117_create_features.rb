class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :spree_features do |t|
      t.string  :name
      t.integer :tenant_id
    end

    create_table :spree_roles_features, id: false do |t|
     t.belongs_to :role
     t.belongs_to :feature
    end
  end
end