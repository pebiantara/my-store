class RemoveUniqShipmentCategoryPerTenant < ActiveRecord::Migration
  def up
    klass   = Spree::ShippingMethodCategory
    remove_index klass.table_name, :name => "unique_spree_shipping_method_categories"
    columns = %w[shipping_category_id shipping_method_id tenant_id]
    add_index klass.table_name, columns, name: 'unique_spree_shipping_method_categories', unique: true
  end

  def down
    klass   = Spree::ShippingMethodCategory
    remove_index klass.table_name, :name => "unique_spree_shipping_method_categories"
    columns = %w[shipping_category_id shipping_method_id]
    add_index klass.table_name, columns, name: 'unique_spree_shipping_method_categories', unique: true
  end
end