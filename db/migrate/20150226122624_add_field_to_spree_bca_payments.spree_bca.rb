# This migration comes from spree_bca (originally 20150226122128)
class AddFieldToSpreeBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :transaction_description, :text
    add_column :spree_bca_payments, :user_define_value, :string
    add_column :spree_bca_payments, :transaction_id, :string
    add_column :spree_bca_payments, :transaction_type, :string
    add_column :spree_bca_payments, :card_no, :string
    add_column :spree_bca_payments, :acquirer_code, :string
    add_column :spree_bca_payments, :acquirer_merchant_account, :string
    add_column :spree_bca_payments, :acquirer_response_code, :string
    add_column :spree_bca_payments, :acquirer_approval_code, :string
    add_column :spree_bca_payments, :scrub_code, :string
    add_column :spree_bca_payments, :scrub_message, :string
  end
end
