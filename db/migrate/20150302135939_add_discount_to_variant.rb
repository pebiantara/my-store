class AddDiscountToVariant < ActiveRecord::Migration
  def change
    add_column :spree_variants, :discount, :float, default: 0.0
  end
end
