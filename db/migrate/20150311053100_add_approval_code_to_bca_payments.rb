class AddApprovalCodeToBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :approval_code, :string
  end
end
