# This migration comes from spree_bca (originally 20150220084459)
class CreateBcaPayment < ActiveRecord::Migration
  def change
    create_table :spree_bca_payments do |t|
      t.integer   :order_id
      t.string    :klik_pay_code
      t.string    :transaction_no
      t.string    :signature
      t.string    :status
      t.text      :response
      t.integer   :tenant_id
      t.timestamps
    end
  end
end
