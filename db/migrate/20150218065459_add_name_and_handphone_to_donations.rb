class AddNameAndHandphoneToDonations < ActiveRecord::Migration
  def change
    add_column :spree_donations, :name, :string
    add_column :spree_donations, :phone_number, :string
    add_column :spree_donations, :payment_method, :string
  end
end
