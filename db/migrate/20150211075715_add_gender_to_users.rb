class AddGenderToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :gender, :string, default: 'men' #men, women
  end
end
