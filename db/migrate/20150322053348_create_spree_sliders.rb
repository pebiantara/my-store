class CreateSpreeSliders < ActiveRecord::Migration
  def change
    create_table :spree_sliders do |t|
      t.string :title
      t.text   :description
      t.integer :tenant_id
      t.integer :product_id
      t.timestamps
    end
  end
end
