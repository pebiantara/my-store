class CreateJneShippings < ActiveRecord::Migration
  def change
    create_table :spree_jne_shippings do |t|
      t.string  :code_name
      t.string  :state_name
      t.string  :city_name
      t.string  :district_name
      t.decimal :price
      t.integer :tenant_id 
      t.timestamps
    end
  end
end