class AddResponseMessageToSpreeBcaPayments < ActiveRecord::Migration
  def change
    add_column :spree_bca_payments, :response_message, :string
  end
end
