class AddFieldSignInCountingToUser < ActiveRecord::Migration
  def change
    add_column :spree_users, :signin_counting, :integer, :default => 0
  end
end