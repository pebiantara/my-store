# This migration comes from spree_bca (originally 20150226103816)
class AddCardNumberToCreditCards < ActiveRecord::Migration
  def change
    add_column :spree_credit_cards, :card_number, :string
  end
end
