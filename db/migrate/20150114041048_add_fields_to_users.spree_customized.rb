# This migration comes from spree_customized (originally 20150114040425)
class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :name, :string
    add_column :spree_users, :phone_number, :string
    add_column :spree_users, :birth_date, :integer
    add_column :spree_users, :subscribe_news, :boolean    
  end
end
