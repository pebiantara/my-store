class AddFieldsToAddresses < ActiveRecord::Migration
  def change
    add_column :spree_addresses, :city_id, :integer
    add_column :spree_addresses, :sub_district_id, :integer
  end
end
