# This migration comes from spree_customized (originally 20150106040610)
class AddCitiesAndSubDistricts < ActiveRecord::Migration
  def change
    create_table :spree_cities do |t|
      t.integer :tenant_id
      t.integer :state_id
      t.integer :country_id
      t.string  :name
      t.string  :code_name
      t.timestamps
    end

    create_table :spree_sub_districts do |t|
      t.integer   :tenant_id
      t.integer   :city_id
      t.string    :name
      t.string    :code_name
      t.timestamps
    end
  end
end
