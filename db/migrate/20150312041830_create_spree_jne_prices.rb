class CreateSpreeJnePrices < ActiveRecord::Migration
  def change
    create_table :spree_jne_prices do |t|
      t.decimal :reg_price, precision: 8, scale: 2
      t.string  :reg_estimation
      t.decimal :oke_price, precision: 8, scale: 2
      t.string  :oke_estimation
      t.decimal :yes_price, precision: 8, scale: 2
      t.string  :yes_estimation
      t.integer :shipping_id 
      t.integer :tenant_id
      t.timestamps
    end

    remove_column :spree_jne_shippings, :state_name, :string
    remove_column :spree_jne_shippings, :city_name, :string
    remove_column :spree_jne_shippings, :district_name, :string
    remove_column :spree_jne_shippings, :jne_type, :string

    add_column :spree_jne_shippings, :city_id, :integer
    add_column :spree_jne_shippings, :sub_district_id, :integer
  end
end
