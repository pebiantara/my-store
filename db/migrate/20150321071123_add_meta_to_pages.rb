class AddMetaToPages < ActiveRecord::Migration
  def change
    add_column :spree_pages, :meta_title, :string
    add_column :spree_pages, :meta_description, :text 
  end
end
