class AddBornPlaceToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :born_place, :string
  end
end
