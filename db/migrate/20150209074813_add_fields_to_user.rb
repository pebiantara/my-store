class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :spree_users, :address, :string
    add_column :spree_users, :city, :string
    add_column :spree_users, :handphone, :string
    add_column :spree_users, :fax, :string
    add_column :spree_users, :pinbbm, :string
    add_column :spree_users, :status, :boolean, default: true
    add_column :spree_users, :profile_picture, :string
  end
end
