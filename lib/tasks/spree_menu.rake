namespace :menu do
  desc "Generate data menu for each tenant"
  task :generate => :environment do
    Spree::Menu.generate_data
  end
end