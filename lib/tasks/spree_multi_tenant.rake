def do_create_tenant domain, code  
  if domain.blank? or code.blank?
    puts "Error: domain and code must be specified"
    puts "(e.g. rake spree_multi_tenant:create_tenant domain=mydomain.com code=mydomain)"
    exit
  end

  tenant = Spree::Tenant.create!({:domain => domain.dup, :code => code.dup})
  tenant.create_template_and_assets_paths

  #creating admin
  system 'bundle exec rake spree_auth:admin:create'

  #create roles
  tenant.create_tenant_spree_data
  tenant
  

  puts "Please setup #{tenant.domain} to your hosts in /etc/hosts for development"
end