namespace :qrcode do
  desc "Recreate qrcode for all products"
  task :recreate => :environment do
    Spree::Product.generate_qrcode
  end
end