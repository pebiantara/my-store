class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :prepare_load_tenant_files

  def prepare_load_tenant_files
    #disable source per tenant because all store will have same feature just disable from admin
    # Dir.glob(File.join(Rails.root, "/spree/**/*.rb")) do |c|
    #   Rails.configuration.cache_classes ? require(c) : load(c)
    # end
    # Multitenant.current_tenant.active_source if Multitenant.current_tenant

    ActionMailer::Base.default_url_options[:host] = Spree::Config[:site_url]
    ActionMailer::Base.smtp_settings = {
      :address                =>  Spree::Config[:mail_host],
      :port                   =>  Spree::Config[:mail_port],
      :domain                 =>  Spree::Config[:mail_domain],
      :user_name              =>  Spree::Config[:smtp_username],
      :password               =>  Spree::Config[:smtp_password],
      :authentication         =>  Spree::Config[:mail_auth_type],
      :enable_starttls_auto   =>  true
    }
  end
end