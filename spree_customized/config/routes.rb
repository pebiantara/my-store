Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  namespace :admin do

    get "mail_methods/email_users/send" => "mail_methods#email_users", as: :email_users
    post "mail_methods/post_email_users/send" => "mail_methods#post_email_users", as: :post_email_users
    resources :tenants
    resources :sliders
    resources :socials
    resources :promo_banners

    resources :users do
      get :send_email_notification
    end
    resources :donations, only: [:index]

    namespace :jne do
      resources :shippings do
        collection { post :upload }
        collection { get :collection_search_base}
      end
    end
    resources :menu, only: [:index, :create]
    resources :pages
    resources :product_reviews, only: [:index, :edit, :update, :destroy] do
      put :update_status
    end
    resources :states do
      resources :cities
    end

    resources :cities, only: [:index] do
      resources :sub_districts
    end

    resources :sub_districts, only: [:index]

    resources :products do
      collection { post :upload_file}
    end

    resources :taxons

    # resources :users do
    #   # collection do
    #   #   get :index_customer
    #   #   post :create_customer
    #   #   get :new_customer
    #   #   get 'edit_customer/:id/edit', :as => :edit_customer, :action => 'edit_customer'
    #   #   put 'update_customer/:id', :as => :update_customer, :action => 'update_customer'
    #   #   delete 'destroy_customer/:id', :as => :destroy_customer, :action => 'destroy_customer'
    #   # end
    # end

    resources :roles
    resources :features
    resources :home
    resources :users_customers
    resources :reports_customers
    resources :activity_logs do
      collection { get 'show_by_date/:date', :as => :show_by_date, :action => 'show_by_date'}
    end
  end

  resources :pages, only: [:show]
  resources :translations, only: [:show]

  namespace :api, defaults: { format: 'json' } do
  	resources :tenants
    resources :cities, only:[:index, :show]
    resources :sub_districts, only:[:index, :show]

    namespace :nontenant, defaults: { format: 'json' } do
      resources :products, only:[:index, :show]
      resources :users
      get "check_email_exist" => "users#check_email_exist", as: :check_user
      get "get_single_payment_method" => "general#get_single_payment_method", as: :get_single_payment_method
      get "get_single_shipping_method" => "general#get_single_shipping_method", as: :get_single_shipping_method
      get "get_single_shipping_rate" => "general#get_single_shipping_rate", as: :get_single_shipping_rate
      get "get_single_shipping_rate_by_tenant" => "general#get_single_shipping_rate_by_tenant", as: :get_single_shipping_rate_by_tenant
      post "custom_order" => "general#custom_order", as: :custom_order

      concern :order_routes do
        member do
          put :cancel
          put :empty
          put :apply_coupon_code
        end

        resources :line_items
        resources :payments do
          member do
            put :authorize
            put :capture
            put :purchase
            put :void
            put :credit
          end
        end

        resources :shipments, only: [:create, :update] do
          member do
            put :ready
            put :ship
            put :add
            put :remove
          end
        end

        resources :addresses, only: [:show, :update]

        resources :return_authorizations do
          member do
            put :add
            put :cancel
            put :receive
          end
        end
      end

      resources :checkouts, only: [:update], concerns: :order_routes do
        member do
          put :next
          put :advance
        end
      end
      get '/orders/mine', to: 'orders#mine', as: 'my_orders'
      get "/orders/current", to: "orders#current", as: "current_order"
      resources :orders, concerns: :order_routes
      resources :shipments, only: [:create, :update] do
        member do
          put :ready
          put :ship
          put :add
          put :remove
        end
      end
    end

    devise_for :spree_user,
               :class_name => 'Spree::User',
               :controllers => { :sessions => 'spree/api/user_sessions', :passwords => 'spree/api/user_passwords'},
               :skip => [:unlocks, :omniauth_callbacks, :registrations],
               :path_prefix => :api,
               :path => :user
  end

  post 'cart' => "orders#edit", as: :cart_path_post
  get "account/orders" => "users#orders", as: :account_orders
  get "account/addresses" => "users#addresses", as: :account_addresses
  get "account/configuration" => "users#configuration", as: :account_configuration

  resources :products do
    collection do
      get "categories/:category_name" => "products#index", as: :products_category
    end
  end

  devise_scope :spree_user do
    get '/confirmation/:token' => 'user_registrations#confirmation', :as => :confirmation
  end

  get "states_by_country" => "info_addresses#states_by_country", as: :states_by_country
  get "cities_by_country" => "info_addresses#cities_by_country", as: :cities_by_country
  get "cities_by_state" => "info_addresses#cities_by_state", as: :cities_by_state
  get "sub_districts_by_city" => "info_addresses#sub_districts_by_city", as: :sub_districts_by_city
  get "be-a-member/:package_type" => "users#be_a_member", as: :be_a_member
  get "be-a-member/completed/:transaction_no" => "users#be_a_member_completed", as: :be_a_member_completed
  post "submit_be_a_member" => "users#submit_be_a_member", as: :submit_be_a_member
  patch "submit_be_a_member" => "users#submit_be_a_member", as: :submit_be_a_member_update

  resources :product_reviews, only: [:create]
  resources :orders do
    collection do
      post :donation
    end
  end
  resources :donations, only: [:index, :create, :show]
end