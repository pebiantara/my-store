module Spree
  module Core
    module ControllerHelpers
      Order.module_eval do

        # Used in the link_to_cart helper.
        def simple_current_order
          @order ||= Spree::Order.find_by(id: session[:order_id], currency: current_currency)
          if @order && @order.completed?
            @order = nil
            #session[:order_id] = nil
          end
          @order
        end
      end
    end
  end
end