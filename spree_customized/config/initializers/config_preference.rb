Rails.application.config.spree.payment_methods = []
Rails.application.config.spree.payment_methods << Spree::Gateway::PayPalGateway
unless Rails.application.config.spree.calculators.shipping_methods.include?(Spree::Calculator::FreeShipping)
  Rails.application.config.spree.calculators.shipping_methods << Spree::Calculator::FreeShipping
end
unless Rails.application.config.spree.payment_methods.include?(Spree::PaymentMethod::Transfer)
  Rails.application.config.spree.payment_methods << Spree::PaymentMethod::Transfer
end