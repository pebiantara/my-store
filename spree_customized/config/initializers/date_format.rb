class DateTime

  def id_format
    self.strftime("%d/%m/%Y")
  end

  def id_format_with_time
    self.strftime("%d/%m/%Y %H:%M")
  end

  def id_format_text
    self.strftime("%d %B, %Y at %I%P")
  end
end

ActiveSupport::TimeWithZone.class_eval do
  def id_format
    self.strftime("%d/%m/%Y")
  end

  def id_format_with_time
    self.strftime("%d/%m/%Y %H:%M")
  end

  def id_format_text
    self.strftime("%d %B, %Y at %I%P")
  end  
end