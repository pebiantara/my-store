module Spree
  module Api
    module ControllerSetupView
      def self.included(klass)
        klass.class_eval do
          append_view_path File.expand_path("../../../tenants/**/views", File.dirname(__FILE__))
        end
      end
    end
  end
end