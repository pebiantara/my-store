class AddSpreeMenus < ActiveRecord::Migration
  def change
    create_table :spree_menus do |t|
      t.string  :name
      t.string  :key
      t.integer :parent_id
      t.integer :tenant_id
      t.timestamps
    end
  end
end
