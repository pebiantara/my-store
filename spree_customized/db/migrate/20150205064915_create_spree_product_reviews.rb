class CreateSpreeProductReviews < ActiveRecord::Migration
  def change
    create_table :spree_product_reviews do |t|
      t.integer :user_id
      t.integer :product_id
      t.integer :rating
      t.string  :name
      t.string  :email
      t.string  :subject
      t.string  :review
      t.integer :tenant_id
      t.string  :status, default: "new"  #new, published
      t.timestamps 
    end
  end
end
