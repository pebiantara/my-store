class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :name, :string
    add_column :spree_users, :phone_number, :string
    add_column :spree_users, :birth_date, :datetime
    add_column :spree_users, :subscribe_news, :boolean    
  end
end
