object false
child(@tenants => :tenant) do
  attributes *[:id, :domain, :code]
end

if @tenants.respond_to?(:num_pages)
  node(:count) { @tenants.count }
  node(:current_page) { params[:page] || 1 }
  node(:pages) { @tenants.num_pages }
end