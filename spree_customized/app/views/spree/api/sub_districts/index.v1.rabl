object false
child(@sub_districts => :sub_districts) do
  attributes :id, :name, :code_name, :city_id
end
node(:count) { @sub_districts.count }
node(:current_page) { params[:page] || 1 }
node(:pages) { @sub_districts.num_pages }
