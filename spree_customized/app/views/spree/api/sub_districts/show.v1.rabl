object @sub_district
attributes :id, :name, :code_name, :city_id
child :city => :city do
  attributes :id, :name, :code_name
end