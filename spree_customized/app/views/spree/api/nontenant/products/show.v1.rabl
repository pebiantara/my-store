object @product
cache [current_currency, root_object]
attributes *product_attributes

node(:price) { |p| Spree::Product.find_by_id(p.id).price.to_s }
node(:display_price) { |p| Spree::Product.find_by_id(p.id).display_price.to_s }
node(:has_variants) { |p| p.has_variants? }
# add new node new_arrival type boolean
node(:new_arrival) { |p| p.available_on + 1.month > Time.now}
node(:top_product) { |p| p.try(:top_product)}
# add new node brand_code
node(:tenant_code) { |p| p.tenant.code rescue nil }
node(:category_id) { |p| (p.taxons.find_by_taxonomy_id(Spree::Taxonomy.find_by_name_and_store_id('Categories', p.store_id).id).id rescue nil) }
node(:category_name) { |p| (p.taxons.find_by_taxonomy_id((Spree::Taxonomy.find_by_name_and_store_id('Categories', p.store_id).id)).name rescue nil) }
# add new node varian
node(:variant_id) { |p| Spree::Variant.find_by_product_id(p.id).id }

child :master => :master do
  extends "spree/api/variants/small"
end

child :variants => :variants do
  extends "spree/api/variants/small"
end

child :option_types => :option_types do
  attributes *option_type_attributes
end

child :product_properties => :product_properties do
  attributes *product_property_attributes
end