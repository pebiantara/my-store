object @user
cache [root_object]
attributes *user_attributes

node(:tenant) { |p| p.tenant }