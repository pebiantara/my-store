object false
child(@cities => :cities) do
  attributes :id, :name, :code_name, :state_id, :country_id
end
node(:count) { @cities.count }
node(:current_page) { params[:page] || 1 }
node(:pages) { @cities.num_pages }
