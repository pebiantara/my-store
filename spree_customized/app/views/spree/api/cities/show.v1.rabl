object @city
attributes :id, :name, :code_name, :state_id, :country_id
child :state => :state do
  attributes :id, :name, :code_name
end
child :country => :country do
  attributes :id, :name, :country_id
end
child :sub_districts => :sub_districts do
  attributes :id, :name
end