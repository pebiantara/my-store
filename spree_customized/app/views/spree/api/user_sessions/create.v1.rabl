object false
child(@user => :user) do
  attributes :id => :id, :spree_api_key => :auth_token, :name => :name, :email => :email, :birthday => :birthday, :address => :address, :city => :city, :phone => :phone, :postal_code => :postal_code, :gender => :gender, :created_at => :created_at, :updated_at => :updated_at, :picture => :picture
end
node(:status) { |m| @user ? true : false }
node(:message) { |m| @user ? 'success' : "Username and Password doesn't match" }