class Spree::InfoAddressesController < Spree::BaseController

  def states_by_country
    @country = Spree::Country.find_by_id(params[:country_id])
    respond_to do |f|
      f.js {
        if @country
          render json: {
                        status: true,
                        country: {id: @country.id, states_required: @country.states_required},
                        states: @country.states.map{|x| [x.id, x.name]}
                       }
        else 
          render json: {status: false, error_message: 'Country not found!'}
        end
      }
    end
  end

  def cities_by_country
    @country = Spree::Country.find_by_id(params[:country_id])
    respond_to do |f|
      f.js {
        if @country
          render json: {
                        status: true,
                        country: {id: @country.id},
                        states: @country.cities.order(:name).map{|x| [x.id, x.name]}
                       }
        else 
          render json: {status: false, error_message: 'Country not found!'}
        end
      }
    end
  end


  def cities_by_state
    @state = Spree::State.find_by_id(params[:state_id])
    respond_to do |f|
      f.js {
        if @state
          render json: {
                        status: true,
                        cities: @state.cities.map{|x| [x.id, x.name]}
                       }
        else 
          render json: {status: false, error_message: 'State not found!'}
        end
      }
    end
  end

  def sub_districts_by_city
    @city = Spree::City.find_by_id(params[:city_id])
    respond_to do |f|
      f.js {
        if @city
          render json: {
                        status: true,
                        districts: @city.sub_districts.map{|x| [x.id, x.name]}
                       }
        else 
          render json: {status: false, error_message: 'City not found!'}
        end
      }
    end
  end
end