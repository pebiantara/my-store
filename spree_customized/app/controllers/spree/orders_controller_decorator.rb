module Spree
  OrdersController.class_eval do
    before_filter :set_current_order, only: [:donation]
   
    def donation
      donation = Spree::Donation.where(order_id: @order.id, total: params[:donation].to_f, tenant_id: Multitenant.current_tenant.id, user_id: spree_current_user.try(:id)).first_or_initialize
      donation.anonim = true
      if donation.save
        @order.adjustments.donation.destroy_all
        ad = @order.adjustments.build(source_id: donation.id, 
          source_type: Spree::Donation.to_s,
          amount: donation.total,
          label: Spree.t('donation'), mandatory: false, order_id: @order.id)
        if ad.save
          ad.send :update_adjustable_adjustment_total
          updater = Spree::OrderContents.new(@order)
          updater.send :reload_totals
        end
        redirect_to :back
      else
        flash[:error] = Spree.t('flash.message.errors.not_valid_donation')
        redirect_to :back
      end
    end

    private
    def set_current_order
      assign_order_with_lock
    end
  end
end