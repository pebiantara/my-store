module Spree
  class TranslationsController < Spree::BaseController
    def show
      session[:locale] = params[:id].to_sym
      #Spree::Frontend::Config[:locale] = session[:cur_lang]
      #Spree::Backend::Config[:locale] = session[:cur_lang]
      redirect_to :back
    end
  end
end