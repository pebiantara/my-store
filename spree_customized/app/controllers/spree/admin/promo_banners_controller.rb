module Spree
  module Admin
    class PromoBannersController < Spree::Admin::ResourceController
      before_filter :build_image, only: :create

      def index
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(25)      
      end

      def new
        @object.build_promo_banner_image
      end

      private
      def build_image
        @object.build_promo_banner_image unless @object.promo_banner_image
      end
    end
  end
end