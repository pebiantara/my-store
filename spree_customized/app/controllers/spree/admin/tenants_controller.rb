module Spree
  module Admin
    class TenantsController < ResourceController
      before_filter :set_tenant_nil
      before_filter :check_auth

      def index
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(25)
      end

      def new
        @object.build_tenant_configuration
      end

      def edit
        @object.build_tenant_configuration unless @object.tenant_configuration
      end

      private
      def set_tenant_nil
        Multitenant.current_tenant = nil
      end

      def check_auth
        unauthorized unless spree_current_user.as_superadmin?
      end
    end
  end
end