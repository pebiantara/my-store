module Spree
  module Admin
    ProductsController.class_eval do
      def collection
        return @collection if @collection.present?
        params[:q] ||= {}
        params[:q][:deleted_at_null] ||= "1"

        params[:q][:s] ||= "name asc"
        @collection = super
        collection_with_deleted = if Multitenant.current_tenant
          @collection.with_deleted.where(tenant_id: Multitenant.current_tenant.id) if params[:q][:deleted_at_null] == '0'
        else
          @collection.with_deleted if params[:q][:deleted_at_null] == '0'
        end

        @collection = collection_with_deleted if collection_with_deleted
        # @search needs to be defined as this is passed to search_form_for
        @search = @collection.ransack(params[:q])
        @collection = @search.result.
        distinct_by_product_ids(params[:q][:s]).
        includes(product_includes).
        page(params[:page]).
        per(Spree::Config[:admin_products_per_page])

        @collection
      end

      def destroy
        @product = Product.friendly.find(params[:id])
        @product.variants_including_master.destroy_all
        @product.really_destroy!
        flash[:success] = Spree.t('notice_messages.product_deleted')

        respond_with(@product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render_js_for_destroy }
        end
      end
    end
  end
end