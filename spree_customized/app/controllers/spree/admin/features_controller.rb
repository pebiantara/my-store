module Spree::Admin
  class FeaturesController < Spree::Admin::BaseController
    before_filter :find_features, :only => [:show, :edit, :update, :destroy]

    def index
      @permissions = Spree::Feature.search(params[:search]).page(params[:page]).per(10)
    end

    def new
      @permissions = Spree::Feature.new
    end

    def create
      @permissions = Spree::Feature.new(feature_params)
      if @permissions.save
        flash[:notice] = "Successfully saving new record"
        redirect_to admin_features_path
      else
        flash[:error] = "Failed saving new record"
        render action: 'new'
      end
    end

    def edit;end

    def update
      if @permissions.update_attributes(feature_params)
        flash[:notice] = "Successfully perform updates."
        redirect_to admin_features_path
      else
        @permissions.reload
        flash[:error] = "Failed to perform updates."
        render action: 'edit'
      end
    end

    def destroy
       if @permissions.destroy
        flash[:notice] = "Successfully deleting record."
        redirect_to admin_features_path
      else
        flash[:error] = "Failed to delete record."
        redirect_to admin_features_path
      end
    end

    private
    def find_features
      @permissions = Spree::Feature.find_by_id(params[:id]) rescue nil
      flash[:error] = 'Feature not found' and redirect_to admin_features_path unless @permissions
    end

    def feature_params
      params.require(:feature).permit(:name)
    end

  end
end