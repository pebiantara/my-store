module Spree::Admin
  class SocialsController < Spree::Admin::ResourceController

    def index
      @search = @collection.ransack(params[:q])
      @collection = @search.result.page(params[:page]).per(25)      
    end
  end
end