class Spree::Admin::CitiesController < Spree::Admin::BaseController
  helper_method :collection_url
  before_action :define_state
  before_action :define_states
  before_action :define_city, only: [:edit, :update, :destroy]

  def index
    @cities = @state.cities.order(:name)
    respond_to do |f|
      f.html
      f.js {render partial: 'city_list'}
    end
  end

  def new
    @city = Spree::City.new(state_id: @state.id, country_id: @state.country.id)
  end
  
  def create
    @city = Spree::City.new(params_page.merge(tenant_id: Multitenant.current_tenant.id))
    if @city.save
      flash[:success] = flash_message_for(@city, :successfully_created)
      redirect_to admin_state_cities_path(params[:state_id])
    else
      render :new
    end
  end

  def edit
    
  end

  def update
    if @city.update_attributes(params_page)
      flash[:success] = flash_message_for(@city, :successfully_created)
      redirect_to admin_state_cities_path(params[:state_id])
    else
      render :edit
    end
  end

  def destroy
    @city.destroy
  end

  private
    def collection_url(options = {})
      spree.polymorphic_url([:admin, model_class], options)
    end

    def define_city
      @city = Spree::City.find_by_id(params[:id])
      flash[:error] = "City not found" unless @city
      redirect_to admin_state_cities_path(@state) and return unless @city
    end
 
    def define_state
      @state = Spree::State.find_by_id params[:state_id]
    end

    def define_states
      @states = Spree::State.order(:name)
    end

    def params_page
      params.require(:city).permit(:name, :code_name, :state_id, :country_id, :tenant_id)
    end
end