class Spree::Admin::ProductReviewsController < Spree::Admin::ResourceController
  #before_action :check_access, only: [:new, :create, :destroy]
  #before_action :define_review, only: [:edit, :update, :destroy]

  def index
    @product_reviews = Spree::ProductReview.search(params)
  end

  def update_status
    render :back
  end
end