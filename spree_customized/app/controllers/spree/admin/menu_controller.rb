class Spree::Admin::MenuController < Spree::Admin::BaseController

  before_filter :check_user
  before_filter :get_menus

  def index
     # raise 'aaa'
  end

  def create
    menus = params[:menu]
    ids = menus.map{|k, id| id.to_i}
    Spree::Menu.all.update_all active: false
    Spree::Menu.where(id: ids).update_all active: true
    flash[:success] = Spree.t("menu_successfully_updated")
    redirect_to admin_menu_index_path
  end

  private
    def check_user
        #please improve me use can can
      unless try_spree_current_user.try(:as_superadmin?)
        flash[:error] = 'Authorization Failure'
        redirect_to edit_admin_general_settings_path and return
      end
    end
end