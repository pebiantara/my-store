module Spree
  module Admin
    class SlidersController < Spree::Admin::ResourceController
      before_filter :build_image, only: :create

      def index
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(25)      
      end

      def new
        @object.build_slider_image
      end

      private
      def build_image
        @object.build_slider_image unless @object.slider_image
      end
    end
  end
end