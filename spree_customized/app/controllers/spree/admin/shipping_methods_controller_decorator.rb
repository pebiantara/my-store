module Spree
  module Admin
    ShippingMethodsController.class_eval do

      private
      def set_shipping_category
        return true if params["shipping_method"][:shipping_categories] == ""
        if @shipping_method.nil?
          load_resource
        end
        @shipping_method.shipping_categories = Spree::ShippingCategory.where(:id => params["shipping_method"][:shipping_categories])
        @shipping_method.save
        params[:shipping_method].delete(:shipping_categories)
      end

      def set_zones
        return true if params["shipping_method"][:zones] == ""
        @shipping_method.zones = Spree::Zone.where(:id => params["shipping_method"][:zones])
        @shipping_method.save
        params[:shipping_method].delete(:zones)
      end

    end
  end
end