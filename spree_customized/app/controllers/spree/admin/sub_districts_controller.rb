class Spree::Admin::SubDistrictsController < Spree::Admin::BaseController
  helper_method :collection_url
  before_action :define_city
  before_action :define_cities
  before_action :define_sub_district, only: [:edit, :update, :destroy]

  def index
    @sub_districts = @city.sub_districts.order(:name)
    respond_to do |f|
      f.html
      f.js {render partial: 'sub_district_list'}
    end
  end

  def new
    @sub_district = Spree::SubDistrict.new(city_id: @city.id)
  end
  
  def create
    @sub_district = Spree::SubDistrict.new(params_page.merge(tenant_id: Multitenant.current_tenant.id))
    if @sub_district.save
      flash[:success] = flash_message_for(@sub_district, :successfully_created)
      redirect_to admin_city_sub_districts_path(params[:city_id])
    else
      render :new
    end
  end

  def edit
    
  end

  def update
    if @sub_district.update_attributes(params_page)
      flash[:success] = flash_message_for(@sub_district, :successfully_created)
      redirect_to admin_city_sub_districts_path(params[:city_id])
    else
      render :edit
    end
  end

  def destroy
    @sub_district.destroy
  end

  private
    def collection_url(options = {})
      spree.polymorphic_url([:admin, model_class], options)
    end

    def define_sub_district
      @sub_district = Spree::SubDistrict.find_by_id(params[:id])
      flash[:error] = "City not found" unless @sub_district
      redirect_to admin_city_sub_districts_path(@city) and return unless @sub_district
    end
 
    def define_city
      @city = Spree::City.find_by_id params[:city_id]
    end

    def define_cities
      @cities = Spree::City.order(:name)
    end

    def params_page
      params.require(:sub_district).permit(:name, :code_name, :city_id, :tenant_id)
    end
end