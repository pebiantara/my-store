module Spree
  module Admin
    UsersController.class_eval do
      def index
        @collection = @collection.where.not(as_superadmin: true)
        @users = @users.where.not(as_superadmin: true)
        respond_with(@collection) do |format|
          format.html
          format.json { render :json => json_data }
        end
      end
    end
  end
end