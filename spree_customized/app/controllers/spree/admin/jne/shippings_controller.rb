module Spree
  module Admin
    module Jne
      class ShippingsController < Spree::Admin::BaseController
        before_action :define_jne_shipping, only: [:edit, :update, :show, :destroy]
        helper_method :collection_url

        def index
          @jne_shippings = Spree::Jne::Shipping.search(params).page(params[:page]).per(10)
        end

        def new
          @jne_shipping = Spree::Jne::Shipping.new
        end

        def create
          @jne_shipping = Spree::Jne::Shipping.new(params_jne_shipping.merge(tenant_id: Multitenant.current_tenant.id))
          if @jne_shipping.save
            flash[:success] = flash_message_for(@jne_shipping, :successfully_created)
            redirect_to admin_jne_shippings_path
          else
            render :new
          end
        end

        def edit
        end

        def update
          if @jne_shipping.update_attributes(params_jne_shipping)
            flash[:success] = flash_message_for(@jne_shipping, :successfully_updated)
            redirect_to admin_jne_shippings_path
          else
            render :new
          end
        end

        def destroy
          @jne_shipping.destroy
        end

        def show

        end

        def upload
          if params[:upload_data] != nil
            uploaded = Spree::Jne::Shipping.upload_file(params[:upload_data], params[:jne_type])
            redirect_to admin_jne_shippings_path, notice: uploaded[:message]
          else
            flash[:error] = "Select file for Upload"
            redirect_to admin_jne_shippings_path
          end
        end

        def collection_search_base
          data = Spree::Jne::Shipping.search(params)
          @states   = data.collect(&:state_name).uniq
          @cities   = data.collect(&:city_name).uniq
          @district = data.collect(&:district_name).uniq
          render json: {states: @states, cities: @cities, districts: @district}
        end

        private
        def collection_url(options = {})
          spree.polymorphic_url([:admin, model_class], options)
        end

        def define_jne_shipping
          @jne_shipping = Spree::Jne::Shipping.find(params[:id])
        end

        def params_jne_shipping
          params.require(:jne_shipping).permit(:state_name, :city_name, :district_name, :code_name, :jne_type)
        end

        def check_access
          unless try_spree_current_user.try(:as_superadmin?)
            flash[:error] = 'Authorization Failure'
            redirect_to admin_jne_shippings_path and return
          end
        end
      end
    end
  end
end