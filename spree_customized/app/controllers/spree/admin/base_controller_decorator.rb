module Spree
  module Admin
    BaseController.class_eval do
      #include TenantAccess
      before_filter :set_default_country
      before_filter :get_menus
      #before_filter :prepend_views_admin
      helper_method :show_menu?
      before_filter :set_current_user
      before_filter :set_locale_id_for_admin

    
      def set_locale_id_for_admin
        I18n.locale = :id
      end

      def set_current_user
        Spree::User.current_user = spree_current_user
      end

      def tenant_active
        Spree::Tenant.find_by_domain(request.host)
      end

      def set_default_country
        country = Spree::Country.where(tenant_id: tenant_active.id).ordered.first if tenant_active
        Spree::Config.set :default_country_id, country.id if country
      end

      def get_menus
        @menus = Spree::Menu.actived.order(:name).collect(&:key)
      end

      def show_menu?(key)
        @menus.include?(key)
      end

      # def cur_tenant
      #   Spree::Tenant.find_by_domain(request.host)
      # end

      # def prepend_views_admin
      #   path = "spree_customized/app/views"
      #   prepend_view_path(path)
      #   Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path

      #   path = "spree_customized/tenants/#{cur_tenant.code}/views"

      #   #remove othe tenant views
      #   Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

      #   prepend_view_path(path)
      #   Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
      # end
    end
  end
end