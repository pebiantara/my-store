class Spree::Admin::PagesController < Spree::Admin::ResourceController
  def index
    @pages = Spree::Page.order(:title).page(params[:page]).per(10)
  end

  private
    def find_resource
      Spree::Page.friendly.find(params[:id])
    end
end