module Spree
  module Admin
    MailMethodsController.class_eval do
      def email_users; end

      def post_email_users
        users = if params[:mail_users].present? 
          Spree::User.where(id: params[:mail_users].to_s.split(",").push(0))
        else
          Spree::User.customers.where.not(id: spree_current_user.id)
        end
        subject = params[:mail_subject]
        content = params[:mail_content]
        users.each do |user|
          user = Spree::UserMailer.delay(queue: 'shippings').send_mail(user.email, subject, content)
        end if users.any?
        
        flash[:notice] = if users.any?
          "Email berhasil dikirim"
        else
          "Tidak ada email yang dituju"
        end
        redirect_to :admin_email_users
      end
    end
  end
end