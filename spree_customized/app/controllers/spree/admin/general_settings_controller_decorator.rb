module Spree::Admin
  GeneralSettingsController.class_eval do
    before_filter :update_logo, only: [:update]
    def edit
      @preferences_general = [:site_name, :default_seo_title, :default_meta_keywords,
                              :default_meta_description, :site_url, :default_locale, :company_profile_site, 
                              :store_address, :merchant_email, :merchant_phone, :slider, :store_name]
      @preferences_security = [:allow_ssl_in_production,
                               :allow_ssl_in_staging, :allow_ssl_in_development_and_test,
                        :check_for_spree_alerts, :enabled_frontend, :user_confirm]
      @preferences_currency = [:display_currency, :hide_cents]
    end
 
    def update_logo
      tenant_attributes = params.delete(:tenant)
      if tenant_attributes
        if Multitenant.current_tenant.update_attributes(tenant_attributes)
          image_url = Multitenant.current_tenant.logo.attachment.url
          Spree::Config[:logo] = image_url
          Spree::Config[:admin_interface_logo] = image_url
        end
      end
    end
  end
end