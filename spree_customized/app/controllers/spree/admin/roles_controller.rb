class Spree::Admin::RolesController < Spree::Admin::BaseController
   before_filter :find_role, :only => [:edit, :update, :destroy]
   before_filter :load_permissions
   before_filter :prepare_features, only: [:new, :edit]

   def index
       @roles = Spree::Role.search(params[:search]).page(params[:page]).per(10)
    end

    def new
      @roles = Spree::Role.new
    end

    def create
      @roles = Spree::Role.new(role_params)
      if @roles.save
        @roles.features = Spree::Feature.where(id: params[:role][:feature_ids].flatten.uniq) unless params[:role][:feature_ids].blank?
        redirect_to admin_roles_path, notice: 'User group was successfully created.'
      else
        @features = Spree::Feature.all
        render :new
      end
    end

    def edit;end

    def update
      if @roles.update(role_params)
        @roles.features = params[:role][:feature_ids].blank? ? [] : Spree::Feature.where(id: params[:role][:feature_ids].flatten.uniq)
        redirect_to  admin_roles_path, notice: 'User group was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
       if @roles.destroy
        flash[:notice] = "Successfully deleting user group."
        redirect_to admin_roles_path
      else
        flash[:error] = "Failed to delete user group."
        redirect_to admin_roles_path
      end
    end

    private
    def prepare_features
      @features = Spree::Feature.all
    end

    def find_role
      @roles = Spree::Role.find_by_id(params[:id]) rescue nil
      flash[:error] = 'User group not found' and redirect_to admin_roles_path unless @roles
    end

    def role_params
      paramd = {controller: params[:controller].split("/").last, action: params[:action]}
      params.require(:role).permit(:name).merge!(paramd)
    end

    def load_permissions
      @permissions = Spree::Feature.all
    end
end