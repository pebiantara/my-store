module Spree::Admin
  class DonationsController < Spree::Admin::BaseController
    def index
      donations = Spree::Donation.order("created_at DESC")
      @total_donation = donations.sum(:total)
      @donations = donations.page(params[:page]).per(20)
      @sub_total = @donations.sum(:total)
    end
  end
end