class Spree::ProductReviewsController < Spree::BaseController
  helper 'spree/products'
  
  #before_filter :authorize_user
  skip_before_filter :verify_authenticity_token
  before_filter :define_product
  rescue_from ActiveRecord::RecordNotFound, :with => :render_404

  def create
    @product_review = Spree::ProductReview.new(product_review_params.merge(user_id: spree_current_user.id, email: spree_current_user.email))
    flash.now[:success] = "Terima kasih atas review anda, kami akan mempublish review anda segera" if @product_review.save    

    respond_to do |f|
      f.html {redirect_to product_path(@product.slug)}
      f.js
    end
  end

  private
  def product_review_params
    params.require(:product_review).permit(:email, :name, :rating, :subject, :review, :product_id)
  end

  def define_product
    @product = Spree::Product.find_by_id(params[:product_review][:product_id])
  end
end