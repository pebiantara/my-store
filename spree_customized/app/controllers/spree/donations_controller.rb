module Spree
  class DonationsController < Spree::StoreController
    #before_filter :checking_user
    def index
      @donation = Spree::Donation.new(name: spree_current_user.try(:name), email: spree_current_user.try(:email), phone_number: spree_current_user.try(:phone_number)) 
      @available_payment_methods = Spree::PaymentMethod.where(active: true)
    end

    def create
      additional = {user_id: spree_current_user.try(:id), tenant_id: Multitenant.current_tenant.id}
      @donation = Spree::Donation.new(params_permitted.merge(additional))
      @available_payment_methods = Spree::PaymentMethod.where(active: true)
      if @donation.save
        #this is should be redirect to payment for donation
        #flash[:success] = Spree.t('flash.message.donation_thanks')
        redirect_to "/payments/klikpay_inquiry?donation_id=#{@donation.id}"
      else
        flash[:error] = Spree.t('flash.message.errors.not_valid_donation')
        render :index
      end
    end

    def params_permitted
      params.require(:donation).permit(:name, :email, :payment_method, :total, :phone_number, :payment_method_id, :anonim)
    end

    def show
      @donation = Spree::Donation.find_by_id(params[:id])
      redirect_to donations_path and return unless @donation
    end

    private
    def checking_user
      flash[:success] = Spree.t('flash.message.donation_need_to_sign_in')
      redirect_to login_path unless spree_current_user
    end
  end
end