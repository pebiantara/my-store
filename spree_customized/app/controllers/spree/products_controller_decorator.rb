module Spree
  ProductsController.class_eval do
    def index
      @searcher = build_searcher(params)
      @products = @searcher.retrieve_products.order("spree_products.updated_at DESC")
      @taxonomies = Spree::Taxonomy.includes(root: :children)
    end
  end
end