#require '/app/controllers/concern/spree/tenant_access'
module Spree
  BaseController.class_eval do
    include TenantAccess

    prepend_before_filter :prepend_views, :check_current_tenant, :set_default_locale
    before_filter :check_access_frontend
    before_filter :define_pages
    before_filter :get_related_products
    before_filter :initial_instances

    private
    def initial_instances
      @groups = Spree::Taxonomy.all
    end

    def get_related_products
      @related_products = if controller_name.eql?('products') && action_name.eql?('show')
        product = Spree::Product.friendly.find(params[:id])
        Spree::Product.joins(:taxons).where.not(id: product.id).where("spree_taxons.id in(?)", product.taxon_ids.push(0)).last(15) if product
      end
    end

    def set_user_language
      I18n.locale = if session.key?(:locale) && SpreeI18n::Config.supported_locales.include?(session[:locale].to_sym)
        session[:locale]
      elsif respond_to?(:config_locale, true) && !config_locale.blank?
        config_locale
      else
        Rails.application.config.i18n.default_locale || I18n.default_locale
      end
    end

    def set_default_locale
      if SpreeI18n::Config.supported_locales.include?(Spree::Config[:default_locale].to_sym)
        Rails.application.config.i18n.default_locale = Spree::Config[:default_locale].to_sym
        I18n.default_locale = Spree::Config[:default_locale].to_sym
      end
    end

    def tenant_active
      Spree::Tenant.find_by_domain(request.host)
    end

    def check_current_tenant
      Multitenant.current_tenant = tenant_active unless current_tenant
    end

    def check_access_frontend
      return if params[:controller].include?('admin')
      unless Spree::Config.preferences[:enabled_frontend]
        not_found
      end
    end

    def define_pages
      #fix me this method for salihara but not load in base decorator folder per tenant
      return unless Multitenant.current_tenant.code.eql?('salihara')
      @top = Spree::Page.where(position: 'top').order(:id).limit(10) unless @top
      @bottom = Spree::Page.where(position: 'bottom').order(:id).limit(10) 
      @categories = Spree::Taxon.where("lower(name) = ?", "categories").first.taxon_childs.order(:id) rescue nil
    end
  end
end