module Spree
  CheckoutController.class_eval do
    
    def load_order_with_lock
      @order = current_order(lock: true) 
      redirect_to spree.cart_path and return unless @order
      if params[:state]
        redirect_to checkout_state_path(@order.state) if @order.can_go_to_state?(params[:state]) && !skip_state_validation?
        @order.state = params[:state]
      end
    end 

    def skip_state_address?
      return false if Multitenant.current_tenant.try(:code) != 'salihara'
      @order.only_ticket? && @order.state == 'address'
    end

    def before_delivery
      return if params[:order].present?

      packages = @order.shipments.map { |s| s.to_package }
      @differentiator = Spree::Stock::Differentiator.new(@order, packages)
      if skip_state_address?
        @differentiator = nil
      end
    end
  end
end