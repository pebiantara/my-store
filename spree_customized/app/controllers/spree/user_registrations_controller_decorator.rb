module Spree
  UserRegistrationsController.class_eval do
    before_filter :prepend_views_devise
    before_filter :check_user_payment, only: :confirmation

    def prepend_views_devise
      path = "spree_customized/tenants/#{Multitenant.current_tenant.code}/views"
      #remove othe tenant views
      Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

      prepend_view_path(path)
      Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
    end

    def confirmation
      @user = Spree::User.find_by_confirmation_token(params[:token])
      if @user
        @user.confirm!
        flash[:success] = "Your account has been confirmed."
        sign_in(:spree_user, @user)
        associate_user
        redirect_to root_path
      else
        flash[:error] = "Confirmation token not valid."
        redirect_to root_path
      end
    end

    private
    def build_resource(*args)
      super
      if session[:omniauth]
        @spree_user.apply_omniauth(session[:omniauth])
        @spree_user.skip_confirmation!
      end
      @spree_user
    end

    def check_user_payment
      return unless tenant.try(:code).eql?('salihara')
      user = Spree::User.find_by_confirmation_token(params[:token])
      if user && user.package_type.present? && !user.payment_status.eql?('paid')
        flash[:error] = Spree.t('flash.message.errors.payment_not_complete')
        redirect_to root_path and return
      end
    end
  end
end