class Spree::PagesController < Spree::StoreController
  helper 'spree/products'

  def show
    @page = Spree::Page.friendly.find(params[:id])
  end
end