class Spree::Api::SubDistrictsController < Spree::Api::BaseController
  skip_before_filter :check_for_user_or_api_key
  skip_before_filter :authenticate_user

  def index
    @sub_districts = Spree::SubDistrict.accessible_by(current_ability, :read).ransack(params[:q]).result.
    includes(:city).order('name ASC').
    page(params[:page]).per(params[:per_page])
    sub_district = Spree::SubDistrict.order("updated_at ASC").last
    if stale?(sub_district)
      respond_with(@sub_districts)
    end
  end

  def show
    @sub_district = Spree::SubDistrict.accessible_by(current_ability, :read).find(params[:id])
    respond_with(@sub_district)
  end
end