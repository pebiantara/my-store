class Spree::Api::CitiesController < Spree::Api::BaseController
  skip_before_filter :check_for_user_or_api_key
  skip_before_filter :authenticate_user

  def index
    @cities = Spree::City.accessible_by(current_ability, :read).ransack(params[:q]).result.
    includes(:state).order('name ASC').
    page(params[:page]).per(params[:per_page])
    city = Spree::City.order("updated_at ASC").last
    if stale?(city)
      respond_with(@cities)
    end
  end

  def show
    @city = Spree::City.accessible_by(current_ability, :read).find(params[:id])
    respond_with(@city)
  end
end