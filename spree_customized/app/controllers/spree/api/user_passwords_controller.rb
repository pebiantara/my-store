class Spree::Api::UserPasswordsController < Spree::Api::BaseController
  skip_before_filter :authenticate_user
  before_filter :set_tenant

  def create
    user = Spree::User.find_for_authentication(:email => params[:email])
    user = user.tenant == @tenant ? user : nil
    @status = user.send_reset_password_instructions if user

    @message = @status.present? ? 'Please check your email to reset your password.' : "Your email doesn't registered"

    respond_with(@status)
  end

  def update
    user = Spree::User.find_for_authentication(spree_api_key: params[:auth_token])
    user = user and user.tenant == @tenant ? user : nil

    if user and user.tenant == @tenant
      if user.valid_password?(params[:current_password])
        @status = user.update_attributes password: params[:new_password], password_confirmation: params[:new_password]
        @message = "Your passsword has been changed."
      else
        @message = "API Key and Password doesn't match"
      end
    else
      @message = 'Wrong API Key'
    end

    respond_with(@status)
  end

  private

    def set_tenant
      @tenant = Spree::Tenant.find_by_domain(request.host)
    end
end