module Spree
  module Api
    class Nontenant::ProductsController < Nontenant::BaseController

      def index
        if params[:ids]
          @products = global_product_scope.where(:id => params[:ids].split(","))
          @products = @products.distinct.page(params[:page]).per(params[:per_page])
        else
          conditions = []
          conditions << "spree_products.id IN (SELECT product_id FROM spree_products_taxons WHERE taxon_id IN (#{params[:category_ids]}))" if params[:category_ids].present?
          if params[:tenant_code].present?
            tenant_id = Spree::Tenant.find_by_code(params[:tenant_code]).id
            conditions << "spree_products.tenant_id=#{tenant_id}"
          end
          conditions << "spree_products.top_product IS TRUE" if params[:top_product] == '1'

          @products = global_product_scope.where(conditions.join (' AND ')).order("spree_products.id ASC").ransack(params[:q]).result.distinct.page(params[:page])
            .per(params[:per_page])
        end
        expires_in 15.minutes, :public => true
        headers['Surrogate-Control'] = "max-age=#{15.minutes}"
      end

      def show
        @product = find_product(params[:id])
        expires_in 15.minutes, :public => true
        headers['Surrogate-Control'] = "max-age=#{15.minutes}"
        headers['Surrogate-Key'] = "product_id=1"
      end

      private

        def global_product_scope
          variants_associations = [{ option_values: :option_type }, :default_price, :prices, :images]
          if current_api_user.has_spree_role?("admin")
            scope = Product.with_deleted.accessible_by(current_ability, :read)
            unless params[:show_deleted]
              scope = scope.not_deleted
            end
          else
            scope = Product.accessible_by(current_ability, :read).active
          end
          scope = scope.includes(:properties, :option_types, variants: variants_associations, master: variants_associations)
          scope
        end

        def product_params
          product_params = params.require(:product).permit(permitted_product_attributes)
          if product_params[:taxon_ids].present?
            product_params[:taxon_ids] = product_params[:taxon_ids].split(',')
          end
          product_params
        end

        def variants_params
          variants_key = if params[:product].has_key? :variants
            :variants
          else
            :variants_attributes
          end

          params.require(:product).permit(
            variants_key => [permitted_variant_attributes, :id],
          ).delete(variants_key) || []
        end

        def option_types_params
          params[:product].fetch(:option_types, [])
        end

        def set_up_shipping_category
          if shipping_category = params[:product].delete(:shipping_category)
            id = ShippingCategory.find_or_create_by(name: shipping_category).id
            params[:product][:shipping_category_id] = id
          end
        end
    end
  end
end
