module Spree
  module Api
    class Nontenant::UsersController < Nontenant::BaseController
      def create
        tenant_id = Spree::Tenant.find_by_code(params[:tenant_code]).try(:id)
        @role = Spree::Role.where("tenant_id = ? And name='user'", tenant_id).first
        @user = Spree::User.new(:email => params[:email], :password => params[:password], :password_confirmation => params[:password], :tenant_id => tenant_id, :confirmed_at => Time.now)
        if @user.save
          @role.users << @user
          respond_with(@user, default_template: :show, status: 201)
        else
          render :text => { :exception => @user.errors }.to_json,
                 :status => 200
          false
        end
      end

      def show
        @user = Spree::User.find(params[:id])
        expires_in 15.minutes, :public => true
        headers['Surrogate-Control'] = "max-age=#{15.minutes}"
        headers['Surrogate-Key'] = "product_id=1"
      end

      def check_email_exist
        tenant_id = Spree::Tenant.find_by_code(params[:tenant_code]).try(:id)
        #@user = Spree::User.where("tenant_id = ? And email= ? ", tenant_id, params[:email]).first
        @user = Spree::User.where("email= ? ", params[:email]).first
        if @user.present?
          render :text => { :status => "already taken", :user_id => @user.id }.to_json,
                 :status => 201
          false
        else
          render :text => { :status => "not found", :user_id => "" }.to_json,
                 :status => 200
          false
        end
      end
    end
  end
end
