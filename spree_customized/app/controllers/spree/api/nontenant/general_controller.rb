module Spree
  module Api
    class Nontenant::GeneralController < Nontenant::BaseController
      def get_single_payment_method
        payment_method = Spree::PaymentMethod.where("tenant_id = ?", params[:tenant_id]).first
        if payment_method.present?
          render :text => { :id => payment_method.id, :name => payment_method.name, :description => payment_method.description }.to_json,
                 :status => 201
          false
        else
          render :text => { :status => "not found", :id => "" }.to_json,
                 :status => 200
          false
        end
      end

      def get_single_shipping_method
        shipping_method = Spree::ShippingMethod.where("tenant_id = ?", params[:tenant_id]).first
        if shipping_method.present?
          render :text => { :id => shipping_method.id, :name => shipping_method.name}.to_json,
                 :status => 201
          false
        else
          render :text => { :status => "not found", :id => "" }.to_json,
                 :status => 200
          false
        end
      end

      def get_single_shipping_rate
        shipping_rate = Spree::ShippingRate.where("shipping_method_id = ?", params[:shipping_method_id]).first
        if shipping_rate.present?
          render :text => { :id => shipping_rate.id, :cost => shipping_rate.cost}.to_json,
                 :status => 201
          false
        else
          render :text => { :status => "not found", :id => "" }.to_json,
                 :status => 200
          false
        end
      end

      def get_single_shipping_rate_by_tenant
        shipping_method = Spree::ShippingMethod.where("tenant_id = ?", params[:tenant_id]).first

        shipping_rate = Spree::ShippingRate.where("shipping_method_id = ?", shipping_method.id).first
        if shipping_rate.present?
          render :text => { :id => shipping_rate.id,  :shipment_id => shipping_rate.shipment_id, :cost => shipping_rate.cost}.to_json,
                 :status => 201
          false
        else
          render :text => { :status => "not found", :id => "" }.to_json,
                 :status => 200
          false
        end
      end

      def custom_order
        if params[:tenant_code].present?
          tenant_id = Spree::Tenant.find_by_code(params[:tenant_code]).id
        end
        shipping_cost = 10000;
        @order = Spree::Core::Importer::Order.import(current_api_user, order_params)
        if tenant_id.present? || params[:user_id].present?
          shipping_method = Spree::ShippingMethod.where("tenant_id = ?", tenant_id).first
          @order.tenant_id = tenant_id
          if params[:user_id]
            user = Spree::user_class.find_by_id(params[:user_id])
            @order.user_id = params[:user_id]
            @order.email = user.email
            @order.state = "complete"
            @order.payment_state = "balance_due"
            @order.shipment_state = "pending"            
            @order.shipping_method_id = shipping_method.try(:id)
          end
          if @order.save
            variant = Spree::Variant.find(params[:line_item][:variant_id])
            @line_item = @order.contents.add(variant, params[:line_item][:quantity].to_i)
            @line_item.tenant_id = tenant_id
            @order.ensure_updated_shipments if @line_item.save

            @order.bill_address.update_attributes({:tenant_id => tenant_id})
            @order.ship_address.update_attributes({:tenant_id => tenant_id})

            begin
              # add shipment
              shipment_id = create_shipment(@order)
              line_item = @order.line_items.first
              count = params[:line_item][:quantity].to_i
              total_item = 0
              (1..count).step do |i| 
                Spree::InventoryUnit.create({state: "backordered", variant_id:line_item.variant.id, order_id:@order.id, shipment_id: shipment_id, pending: true, line_item_id: line_item.id, tenant_id: tenant_id})
                total_item = total_item + line_item.variant.product.price.to_i
              end
              shipment_total = (variant.weight.to_f * shipping_cost.to_f)
              tax_total = 0
              total_all = (total_item.to_i + shipment_total.to_i + tax_total.to_i)
              ActiveRecord::Base.connection.execute "UPDATE spree_orders SET completed_at='#{Time.now}', item_total='#{total_item}', shipment_total='#{shipment_total.to_i}', additional_tax_total= '#{tax_total.to_i}', total='#{total_all}' WHERE id='#{@order.id}'"
              #add payment
              payment_method = Spree::PaymentMethod.where("tenant_id = ?", tenant_id).first
              @payment = @order.payments.create({amount: total_item, source_id:1, source_type:"Spree::CreditCard", payment_method_id: payment_method.id, state: "pending", tenant_id: tenant_id})
            rescue
            end
          end
        end

        if @order.number.blank?
          render :text => { :status => "Failed to create Order", :id => nil, :number => nil, :item_total => 0, :total => 0, :shipment_total => 0, :additional_tax_total => 0}.to_json, :status => 200
          false
        else
          render :text => { :status => "Order created #{@order.number}", :id => @order.id, :number => @order.number, :item_total => total_item, :total =>  (total_item.to_i + shipment_total.to_i + tax_total.to_i), :shipment_total => shipment_total.to_i, :additional_tax_total => tax_total.to_i}.to_json, :status => 200
          false
        end
      end

      private
        def create_shipment(order)
          variant = Spree::Variant.find(params[:line_item][:variant_id])
          quantity = params[:line_item][:quantity].to_i
          @shipment = order.shipments.create({stock_location_id: 1, tenant_id: order.tenant_id})
          order.contents.add(variant, quantity, nil, @shipment)
          @shipment.refresh_rates
          @shipment.save!

          return @shipment.id
        end

        def deal_with_line_items
          line_item_attributes = params[:order][:line_items]
          line_item_attributes.each_key do |key|
            # need to call .to_hash to make sure Rails 4's strong parameters don't bite
            line_item_attributes[key] = line_item_attributes[key].slice(*permitted_line_item_attributes).to_hash
          end
          @order.update_line_items(line_item_attributes)
        end

        def order_params
          if params[:order]
            params[:order][:payments_attributes] = params[:order][:payments] if params[:order][:payments]
            params[:order][:shipments_attributes] = params[:order][:shipments] if params[:order][:shipments]
            params[:order][:line_items_attributes] = params[:order][:line_items] if params[:order][:line_items]
            params[:order][:ship_address_attributes] = params[:order][:ship_address] if params[:order][:ship_address]
            params[:order][:bill_address_attributes] = params[:order][:bill_address] if params[:order][:bill_address]

            params.require(:order).permit(permitted_order_attributes)
          else
            {}
          end
        end

        def permitted_order_attributes
          if current_api_user.has_spree_role? "admin"
            super << admin_order_attributes
          else
            super
          end
        end

        def permitted_shipment_attributes
          if current_api_user.has_spree_role? "admin"
            super << admin_shipment_attributes
          else
            super
          end
        end

        def admin_shipment_attributes
          [:shipping_method, :stock_location, :inventory_units => [:variant_id, :sku]]
        end

        def admin_order_attributes
          [:import, :number, :completed_at, :locked_at, :channel]
        end

        def next!(options={})
          if @order.valid? && @order.next
            render :show, status: options[:status] || 200
          else
            render :could_not_transition, status: 422
          end
        end

        def find_order(lock = false)
          @order = Spree::Order.lock(lock).find_by!(number: params[:id])
        end

        def before_delivery
          @order.create_proposed_shipments
        end

        def order_id
          super || params[:id]
        end

    end
  end
end
