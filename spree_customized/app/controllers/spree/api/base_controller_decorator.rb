module Spree
  module Api
    BaseController.class_eval do
      prepend_before_filter :prepare_load_tenant_files
      prepend_before_filter :check_current_tenant
      prepend_around_filter :tenant_scope
      before_filter :prepend_views_api
      skip_before_filter :authorize_for_order

      private
      def tenant_active
        Spree::Tenant.find_by_domain(request.host)
      end

      def check_current_tenant
        Multitenant.current_tenant = tenant_active #unless tenant_active
      end

      def prepare_load_tenant_files
        #file on folder spree copied from gem it's self
        Dir.glob(File.join(Rails.root, "/spree/**/*.rb")) do |c|
          Rails.configuration.cache_classes ? require(c) : load(c)
        end
        Multitenant.current_tenant.active_source if Multitenant.current_tenant
      end

      def tenant_scope
        tenant = Spree::Tenant.find_by_domain(request.host)
        raise 'DomainUnknown' unless tenant
        #tenant.active_source

        #overide if products scope without tenant, product will be handle on product_scope
        unless controller_name.include?('products')
          SpreeMultiTenant.with_tenant tenant do
            yield
          end
        else
          yield
        end
      end

      def product_scope
        variants_associations = [{ option_values: :option_type }, :default_price, :prices, :images]
        if current_api_user.has_spree_role?("admin")
          scope = Product.with_deleted.accessible_by(current_ability, :read)
          unless params[:show_deleted]
            scope = scope.not_deleted
          end
        else
          scope = Product.accessible_by(current_ability, :read).active
        end
        tenant = Spree::Tenant.find_by_domain(request.host)
        scope = scope.where(tenant_id: tenant.id) if tenant
        #scope = scope.includes(:properties, :option_types, variants: variants_associations, master: variants_associations)
        scope
      end

      def cur_tenant
        Spree::Tenant.find_by_domain(request.host)
      end

      def prepend_views_api
        path = "spree_customized/tenants/#{cur_tenant.code}/views"

        #remove othe tenant views
        Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

        prepend_view_path(path)
        Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
      end
    end
  end
end