module Spree
  module Api
    LineItemsController.class_eval do
      def create
        l_item = nil
        if params[:order][:line_items]
          deal_with_multiple_line_items
          @order = order
          #please improve me use use respons_with spree method
          render '/spree/api/orders/show' and return
          #respond_with(order, default_template: :show, status: 201).inspect
        else
          variant = Spree::Variant.find(params[:line_item][:variant_id])
          @line_item = order.contents.add(variant, params[:line_item][:quantity])
          if @line_item.save
            @order.ensure_updated_shipments
            respond_with(@line_item, status: 201, default_template: :show)
          else
            invalid_resource!(@line_item)
          end
        end
      end

      def deal_with_multiple_line_items
        params[:order][:line_items].each do |index, line_item|
          variant = Spree::Variant.find(line_item[:variant_id])
          qty = line_item[:quantity]
          if variant
            l_item = order.line_items.where(variant_id: variant.id).last
            if l_item
              line_items_attrs = { line_items_attributes: { id: l_item.id, quantity: qty }}
              if order.contents.update_cart(line_items_attrs)
                order.ensure_updated_shipments
                l_item.reload
              end
            else
              l_item = order.contents.add(variant, qty)
              if l_item.save
                order.ensure_updated_shipments
              end
            end
          end
        end
      end
    end
  end
end