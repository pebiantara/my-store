class Spree::Api::TenantsController < Spree::Api::BaseController
  def index
  	@tenants = Spree::Tenant.where("domain ILIKE ?","%#{params[:name]}%").order('code ASC')
    if params[:page] || params[:per_page]
      @tenants = @tenants.page(params[:page]).per(params[:per_page])
    end
    respond_with(@tenants)
  end
end