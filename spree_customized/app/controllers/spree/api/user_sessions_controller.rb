class Spree::Api::UserSessionsController < Spree::Api::BaseController
  skip_before_filter :authenticate_user

  def create
    @tenant = Spree::Tenant.find_by_domain(request.host)
    @user = Spree::User.api_authenticate_for_tenant(@tenant, params[:username], params[:password])

    @user.generate_spree_api_key! if @user and @tenant and !@user.spree_api_key

    respond_with(@user)
  end
end