module Spree
  module Api
    ShipmentsController.class_eval do

      def find_and_update_shipment
        if @order.present?
          @shipment = @order.shipments.accessible_by(current_ability, :update).find_by!(number: params[:id])
        else
          @shipment = Spree::Shipment.find_by!(number: params[:id])
        end
        @shipment.update_attributes(shipment_params)
        @shipment.reload
      end
    end
  end
end