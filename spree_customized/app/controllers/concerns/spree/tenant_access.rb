module Spree
  module TenantAccess
    extend ActiveSupport::Concern

    included do
      #attr_reader :month, :contests
      before_filter :check_tenant_access
      before_filter :prepend_template_views
    end

    def check_tenant_access
      return if admin_controller?
      not_found if cur_tenant.expired? || !cur_tenant.is_active?
    end

    def cur_tenant
      Multitenant.current_tenant
    end

    def not_found
      #raise ActionController::RoutingError.new('Not Found')
      #render nothing: true, status: 404
      respond_to do |type|
        type.html { render :status => :not_found, :file    => "#{::Rails.root}/public/404", :formats => [:html], :layout => nil}
        type.all  { render :status => :not_found, :nothing => true }
      end
    end



    def admin_controller?
      params[:controller].include?('admin')
    end

    def prepend_template_views
      return if admin_controller?
      #fix me with dynamic template
      path = "spree_customized/templates/#{Spree::Config[:template]}/"
      #remove othe tenant views
      Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'templates' and registered_path != path}
      prepend_view_path(path)
      Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
    end

    def prepend_views
      return unless Multitenant.current_tenant
      path = "spree_customized/tenants/#{Multitenant.current_tenant.code}/views"

      #remove othe tenant views
      Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

      prepend_view_path(path)
      Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
    end
  end
end