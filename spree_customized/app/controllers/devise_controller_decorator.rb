DeviseController.class_eval do
  prepend_before_filter :prepend_template_views
  before_filter :initial_instances

  def initial_instances
    return unless params[:controller].include?('admin')
    @groups = Spree::Taxonomy.all
  end

  def tenant
    tenant = Spree::Tenant.find_by_domain(request.host)
    Multitenant.current_tenant = tenant
    tenant
  end
  
  #this is for templates per tenant if in the future some tenant will be have different feature
  # def prepend_views
  #   path = "spree_customized/tenants/#{tenant.code}/views"
  #   #remove othe tenant views
  #   Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

  #   prepend_view_path(path)
  #   Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
  # end

  def prepend_template_views
    #fix me with dynamic template
    path = "spree_customized/templates/#{Spree::Config[:template]}/"
    #remove othe tenant views
    Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'templates' and registered_path != path}
    prepend_view_path(path)
    Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
  end

  private
    def selected_layout
      return "spree/layouts/spree_application" unless params[:controller].include?('admin')
      'spree/admin/layouts/admin/login'
    end
end