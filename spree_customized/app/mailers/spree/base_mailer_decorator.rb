module Spree
  BaseMailer.class_eval do
    layout :set_layout_mailer

    def set_layout_mailer
      "/spree/layouts/mailer"
    end     
  end
end