module Spree
  UserMailer.class_eval do
    def confirmation_instructions(record, token, opts={})
      @token = token
      #devise_mail(record, :confirmation_instructions, opts)
      @resource = record
      mail(:to => @resource.email, :from => from_address,
          :subject => Spree::Config[:site_name] + ' Confirmation')
    end

    def member_confirmation(member)
      @member = member
      mail(:to => member.email, :from => from_address,
          :subject => Spree::Config[:site_name] + ' Confirmation Be a member')
    end

    def notification_expire(member)
      @member = member
      mail(:to => member.email, :from => from_address,
          :subject => Spree::Config[:site_name] + ' Expired account')
    end

    def send_mail(user, subject, content)
      @user = user
      @subject = subject
      @content = content
      mail(:to => @user, :from => from_address, :subject => subject)
    end
  end
end
