Deface::Override.new(:virtual_path => 'spree/products/show',
  :name => 'add_review_content',
  :insert_bottom => "[data-hook='product_show']",
  :partial => "spree/shared/reviews")
