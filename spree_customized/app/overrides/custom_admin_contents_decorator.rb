Deface::Override.new(virtual_path: 'spree/admin/shared/_content_header',
  name: 'add_link_email_to_users', 
  insert_top: ".inline-menu",
  text: "<li><%= link_to_with_icon 'icon-envelope-alt', Spree.t('admin.mail_methods.send_email_to_users'),admin_email_users_path, :title => Spree.t('admin.mail_methods.send_email_to_users'), :class => 'send_mail button no-text' %></li>")