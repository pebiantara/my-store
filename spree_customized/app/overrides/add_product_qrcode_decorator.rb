Deface::Override.new(:virtual_path => 'spree/admin/shared/_product_tabs',
  :name => 'add_product_qrcode',
  :insert_bottom => "[data-hook='admin_product_tabs']",
  :partial => "spree/admin/shared/product_qrcode")

Deface::Override.new(:virtual_path => 'spree/products/show',
  :name => 'add_product_qrcode_frontend',
  :insert_bottom => "[data-hook='product_images']",
  :partial => "spree/shared/product_qrcode")

Deface::Override.new(:virtual_path => 'spree/admin/payments/index',
  :name => 'remove_new_payment_button',
  :replace => "li#new_payment_section",
  :text => "")