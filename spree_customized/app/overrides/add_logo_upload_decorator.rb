Deface::Override.new(:virtual_path => 'spree/admin/general_settings/edit',
  :name => 'add_logo_upload',
  :insert_top => "div#preferences",
  :partial => "spree/admin/shared/upload_logo")

Deface::Override.new(:virtual_path => 'spree/admin/general_settings/edit',
  :name => 'set_enctype_form',
  :replace_contents => "erb[loud]:contains(\"form_tag admin_general_settings_path, :method => :put do\")",
  :text => "form_tag admin_general_settings_path, :enctype => 'multipart/form-data', :method => :put do"
  )
#:set_attributes => "code[erb-loud]:contains(\"form\")",

#rake deface:test_selector['spree/admin/general_settings/edit','erb[loud]:contains("form_tag admin_general_settings_path")']
