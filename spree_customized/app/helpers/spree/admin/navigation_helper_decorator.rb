module Spree
  module Admin
    NavigationHelper.module_eval do
      def load_flash_message
        if flash.present?
          message = flash[:error] || flash[:notice] || flash[:success]
          flash_class = "danger" if flash[:error]
          flash_class = "info" if flash[:notice]
          flash_class = "success" if flash[:success]
          flash_div = content_tag(:div, message, class: "alert alert-#{flash_class} alert-auto-dissapear")
          content_tag(:div, flash_div, class: 'col-md-14')
        end
      end

      def tab(*args)
        options = {:label => args.first.to_s}

        # Return if resource is found and user is not allowed to :admin
        return '' if klass = klass_for(options[:label]) and cannot?(:admin, klass)

        if args.last.is_a?(Hash)
          options = options.merge(args.pop)
        end
        options[:route] ||=  "admin_#{args.first}"

        destination_url = options[:url] || spree.send("#{options[:route]}_path")
        # titleized_label = Spree.t(options[:label], :default => options[:label], :scope => [:admin, :tab]).titleize
        titleized_label = Spree.t(options[:label]).titleize

        css_classes = []

        if options[:icon]
          link = link_to_with_icon(options[:icon], titleized_label, destination_url)
          css_classes << 'tab-with-icon'
        else
          link = link_to(titleized_label, destination_url)
        end

        selected = if options[:match_path].is_a? Regexp
          request.fullpath =~ options[:match_path]
        elsif options[:match_path]
          request.fullpath.starts_with?("#{admin_path}#{options[:match_path]}")
        else
          args.include?(controller.controller_name.to_sym)
        end
        css_classes << 'selected' if selected

        if options[:css_class]
          css_classes << options[:css_class]
        end
        content_tag('li', link, :class => css_classes.join(' '))
      end      
    end
  end
end
#       def tab(*args)
#         options = {:label => args.first.to_s}

#         # Return if resource is found and user is not allowed to :admin
#         return '' if klass = klass_for(options[:label]) and cannot?(:admin, klass)

#         if args.last.is_a?(Hash)
#           options = options.merge(args.pop)
#         end
#         options[:route] ||=  "admin_#{args.first}"

#         destination_url = options[:url] || spree.send("#{options[:route]}_path")
#         titleized_label = Spree.t(options[:label], :default => options[:label], :scope => [:admin, :tab]).titleize

#         css_classes = []

#         if options[:icon]
#           link = link_to_with_icon(options[:icon], titleized_label, destination_url)
#           css_classes << 'tab-with-icon'
#         else
#           link = link_to(titleized_label, destination_url)
#         end

#         selected = if options[:match_path].is_a? Regexp
#           request.fullpath =~ options[:match_path]
#         elsif options[:match_path]
#           request.fullpath.starts_with?("#{admin_path}#{options[:match_path]}")
#         else
#           args.include?(controller.controller_name.to_sym)
#         end
#         css_classes << 'selected' if selected

#         if options[:css_class]
#           css_classes << options[:css_class]
#         end
#         content_tag('li', link, :class => css_classes.join(' '))
#       end
#     end
#   end
# end