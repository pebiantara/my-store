module Spree
  StoreHelper.module_eval do 
    def cache_key_for_taxons
      max_updated_at = @taxons.maximum(:updated_at).to_i rescue Time.zone.now.to_i
      parts = [@taxon.try(:id), max_updated_at].compact.join("-")
      "taxons/#{parts}"
    end
  end
end
