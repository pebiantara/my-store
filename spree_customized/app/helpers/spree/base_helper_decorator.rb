module Spree
  BaseHelper.module_eval do

    def show_price_discount_html(product)
      html = ""
      html += "<s>" unless product.discount.zero?
      html += display_price(product)
      html += "</s>"
      unless product.discount.zero?
        html += " - "
        html += display_price_with_discount(product)
      end
      raw html
    end

    def display_price_with_discount(product)
      price = product.price_in(current_currency).price
      disc = (price.to_f * (product.discount/100))
      Spree::Money.new(price - disc, { currency: current_currency}).to_html
    end

    def top_menu_mobile
      return unless @groups
      content = '<ul class="submenu" style="display: none;">'
      ids = @groups.map{|x| x.root.id}
      Spree::Taxon.where(id: ids).each do |taxon|
        content +='<li>'
        content +='<ul class="topnav">'
        content += '<li class="level0 nav-6 level-top">'
        content += link_to "/t/"+taxon.permalink, class: 'level-top', title: taxon.name do
          content_tag :span, taxon.name
        end
        if taxon.taxon_childs.any?
          content += get_childs(taxon.taxon_childs)
        end
        content += '</li>'
        content += '</ul>'
        content += '</li>'
      end
      content +='</ul>'
      raw content
    end

    def get_childs(taxons, level=0)
      content = "<ul class=\"level#{level}\" style=\"display: none;\">"
      taxons.each do |taxon|
        content += "<li class='level#{level}'>"
        content += link_to "/t/"+ taxon.permalink, class: 'level-top', title: taxon.name do
          content_tag :span, taxon.name
        end
        if taxon.taxon_childs.any?
          content += get_childs(taxon.taxon_childs, level + 1)
        end
        content += '</li>'        
      end
      content += '</ul>'
      content
    end
  
    def top_menu
      return '' unless @groups
      content = "<ul id='nav' class='hidden-xs'>"
      @groups.each do |taxonomi|
        content += '<li class="level0 level-top first">'
        content += link_to "/t/" + taxonomi.root.permalink, class: 'level-top', title: taxonomi.root.name do
          content_tag :span, taxonomi.root.name 
        end
        content += '<div class="level0-wrapper dropdown-6col astramenu_sub style_wide col-md-12">'
        content += '<div class="level0-wrapper2 row">'
        content += '<div class="nav-block nav-block-center col-md-12 astramenu_container">'
        content += '<ul class="level0">'
        taxonomi.taxons.where.not(parent_id: nil).each_with_index do |taxon, level|
          next if !taxon.parent.parent_id.nil?
          content += '<li class="level1">'
          content += link_to "/t/" + taxon.permalink, class: "level-top", title: taxon.name do
            content_tag :span, taxon.name
          end
          if taxon.taxon_childs.any?
            content += '<ul class="level1">'
            taxon.taxon_childs.each do |child|
              content +='<li class="level2">'
              content += link_to "/t/"+child.permalink, class: "level-top", title: child.name do
                content_tag :span, child.name
              end
              content += "</li>"
            end
            content += '</ul>'
          end
          content += "</li>"
        end
        content += "</ul></div></div></div>"
      end
      content += "</li>"
      content += "</ul>"
      raw content
    end

    def payment_status(bca)
      stat_text = Spree::Bca::Transaction.statuses[bca.response_message["status"]][I18n.locale] rescue ''
      complete_text = bca.response_message["message"][I18n.locale.to_s] rescue ''
      if ["Success", "Sukses"].include?(complete_text)
        language = {id: "Transaksi berhasil di bayar", en: "Transaction succesfully"}
        complete_text = language[I18n.locale]
      end 
      [stat_text, complete_text].delete_if{|x| !x.present?}.join(",")
    end

    def breadcrumbs(taxon, separator="&nbsp;&raquo;&nbsp;")
      return "" if current_page?("/") || taxon.nil?
      separator = raw(separator)
      crumbs = [content_tag(:li, link_to(Spree.t(:home), spree.root_path) + separator)]
      if taxon
        crumbs << content_tag(:li, link_to(Spree.t(:products), products_path) + separator)
        crumbs << taxon.ancestors.collect { |ancestor| content_tag(:li, link_to(ancestor.name , seo_url(ancestor)) + separator) } unless taxon.ancestors.empty?
        crumbs << content_tag(:li, content_tag(:span, link_to(taxon.name , seo_url(taxon))))
      else
        crumbs << content_tag(:li, content_tag(:span, Spree.t(:products)))
      end
      crumb_list = content_tag(:ul, raw(crumbs.flatten.map{|li| li.mb_chars}.join), class: 'inline')
      content_tag(:div, crumb_list, id: 'breadcrumbs', class: 'sixteen columns')
    end


    def clear_description(desc)
      return "" unless desc
      strip_tags(desc).gsub("&nbsp;", "").gsub("\n", "").gsub("\r", '')
    end

    def translation(klass, field)
      obj = klass.translations.find_by_locale(Spree::Frontend::Config.locale)
      obj && eval("obj.#{field}.present?") ? eval("obj.#{field}") : eval("klass.#{field}")
    end

    def taxon_translation(taxon)
      tax = taxon.translations.find_by_locale(Spree::Frontend::Config.locale)
      tax && tax.name.present? ? tax.name : taxon.name
    end

    def taxon_permalink_translation(taxon)
      tax = taxon.translations.find_by_locale(Spree::Frontend::Config.locale)
      tax && tax.permalink.present? ? tax.permalink : taxon.permalink
    end

    def product_description_translation(product)
      prod = product.translations.find_by_locale(Spree::Frontend::Config.locale)
      prod && prod.description.present? ? prod.description : product.description
    end

    def product_slug_translation(product)
      prod = prod.translations.find_by_locale(Spree::Frontend::Config.locale)
      prod && prod.slug.present? ? prod.slug : product.slug
    end

    def get_profile_picture(user)
      src = "/assets/user.jpg"
      if user.profile_picture && user.profile_picture.url
        src = user.profile_picture.url
      end
      image_tag(src,class: "ava")
    end

    def product_title
      if params[:category_name]
        Spree::Taxon.where(permalink: "categories/#{params[:category_name]}").first.try(:name)
      else
        "Semua Produk"
      end
    end

    def list_of_products(products)
      html = ""
      per_row = 3
      first_row = true
      products.each_with_index do |product, i|
        if first_row
          first_row = false
          html += "<div class=\"row clearfix\">"
        end
        html += product_content(product)
        if (i != 0 && i % per_row == 0) || i == products.length - 1
          html += "</div>"
          first_row = true
        end
      end
      raw html         
    end

    def product_content(product)
      content_event = "<p>#{product.event_days}, #{product.event_time}</p><p>#{product.event_place}</p>" if product.is_event?
      content_image = if product.images.any?
        image_tag(product.images.first.attachment.url(:product))
      else
        image_tag("/assets/noimage/large.png")
      end

      "<div class=\"col-sm-3 col-md-3  column product-box simple-tooltip\"  
            data-content=\"
             #{content_event}
             <p>#{product.description.truncate(100)}</p>
               <a href='/products/#{product.friendly_id}' class='link-to'>LIHAT SELENGKAPNYA</a>
               <div class='clear'></div>
               <a href='' class='btn btn-default pull-left'>
               <i class='fa fa-shopping-cart'></i> ADD TO CART</a>
               \" data-title=\"<span class='title'>#{product.name}</span>
               <span class='price-box'>#{display_price(product)}</span>\">
               <div class=\"caption\">
                  <span class=\"title\">#{product.name}</span>
                  <span class=\"price\">#{display_price(product)}</span>
               </div>
               <a href=\"\" class=\"cart-link\"><i class=\"fa fa-plus\"></i></a>
               #{content_image}
            </div>"
    end

    def paginate_custom(object, attrs={})
      cur_page    = object.current_page
      total_pages = object.total_pages
      outer_left  = 3
      outer_right = 3
      min_pages   = cur_page - 3
      max_pages   = cur_page + 3
      window      = 4
      param_name  = attrs[:param_name] ? attrs[:param_name] : "page"
      cur_url     = request.url
  
      if cur_page == 1
        first_page = true
      elsif cur_page == total_pages
        last_page  = true
      end
 
      start_page = min_pages <= 0 ? 1 : min_pages

      prev_link = if cur_url.include?("#{param_name}=#{cur_page}") 
        cur_url.gsub("#{param_name}=#{cur_page}", "#{param_name}=#{cur_page - 1}")  
      elsif cur_url.include?("?") 
        cur_url + "&#{param_name}=#{cur_page - 1}"
      else
        cur_url + "?#{param_name}=#{cur_page - 1}"
      end

      next_link = if cur_url.include?("#{param_name}=#{cur_page}") 
        cur_url.gsub("#{param_name}=#{cur_page}", "#{param_name}=#{cur_page + 1}")  
      elsif cur_url.include?("?") 
        cur_url + "&#{param_name}=#{cur_page + 1}"
      else
        cur_url + "?#{param_name}=#{cur_page + 1}"
      end

      if first_page
        prev_link = "#"
      end

      if last_page
        next_link = "#"
      end

      link_pagination = ""
      (start_page..(window + start_page)).each do |page|
        break if page > total_pages
        page_link = if cur_url.include?("#{param_name}=#{cur_page}") 
          cur_url.gsub("#{param_name}=#{cur_page}", "#{param_name}=#{page}")  
        elsif cur_url.include?("?") 
          cur_url + "&#{param_name}=#{page}"
        else
          cur_url + "?#{param_name}=#{page}"
        end

        link_pagination += "<li class=\"#{'active' if page.eql?(cur_page)}\"><a href=\"#{page_link}\">#{page}</a></li>"
      end

      content = ""
      content +="<div class=\"row clearfix\">
            <div class=\"col-md-4\">
               <span class=\"number-product align-left\">
                  <a href=\"#{prev_link}\"><i class=\"fa fa-long-arrow-left\"></i> Sebelumnya</a>
               </span>
            </div>
            <div class=\"col-md-4 text-center\">
               <ul class=\"pagination\">
                  #{link_pagination}
               </ul>
            </div>
            <div class=\"col-md-4\">
               <span class=\"number-product  align-right\">
                  <a href=\"#{next_link}\"> Selanjutnya <i class=\"fa fa-long-arrow-right\"></i></a>
               </span>
            </div>
         </div>"
      raw content
    end
  end
end