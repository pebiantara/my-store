module HistoryLog
  extend ActiveSupport::Concern

  included do
    cattr_accessor :old_attributes, :current_user, :controller, :action
    has_many :activity_logs, as: :source
    after_save :create_history
    # before_save do
    #   self.old_attributes = self.attributes.to_json
    # end
    before_save :collect_old_data
  end

  def collect_old_data
    self.old_attributes = self.new_record? ? self.to_json : self.class.find_by_id(self.id).to_json
  end

  def create_history
    if Spree::User.current_user != nil
      ip_address = Spree::User.current_user.current_sign_in_ip
      login_at = Spree::User.current_user.current_sign_in_at
      self.activity_logs.build(activity: "#{self.action}\ #{self.controller}", edit_by: try_current_user.try(:id), old_attributes: self.old_attributes, ip_address: ip_address, login_at: login_at).save
    end
  end

  def field_changed
    current_self = self.attributes
    source_changed = activity_logs.last.old_attributes
    fields_changed = []
    current_self.each do |key, val|
      if date_time_fields.include?(get_type_field(key))
        if val && source_changed[key]
          fields_changed << key.to_sym if val && val.strftime("%Y-%m-%d %H:%M:%S") != Time.parse(source_changed[key]).strftime("%Y-%m-%d %H:%M:%S")
        end
      else
        fields_changed << key.to_sym if val != source_changed[key]
      end
    end
    fields_changed - show_field
  end

  def date_time_fields
    [:datetime, :date, :time]
  end

  def show_field
    [:encrypted_password, :confirmation_token, :password_salt, :remember_token, :persistence_token, :perishable_token, :reset_password_token, :sign_in_count, :failed_attempts, :updated_at, :profile_picture, :current_sign_in_ip, :last_sign_in_ip  ]
  end

  def get_type_field(field)
    column_for_attribute(field.to_sym).type
  end

  def try_current_user
     Spree::User.current_user
     #code buat dapatkan current user yang melakukan aktifitas
  end

end