module TenantScoped
  extend ActiveSupport::Concern

  included do
    belongs_to :tenant
    belongs_to_multitenant
    before_save :initial_tenant
  end

  def initial_tenant
    self.tenant_id = Multitenant.current_tenant.try :id unless tenant_id
  end

  class << self
    def unscoped
      r = relation
      r = r.where(:tenant_id => Multitenant.current_tenant.id) if Multitenant.current_tenant
      block_given? ? r.scoping { yield } : r
    end
  end
end