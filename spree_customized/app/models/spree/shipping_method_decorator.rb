module Spree
  ShippingMethod.class_eval do
    def is_jne?
      name.downcase.include?('jne') || admin_name.downcase.include?('jne')
    end

    def self.calculators
      spree_calculators.send(model_name_without_spree_namespace)
    end
  end
end