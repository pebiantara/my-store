module Spree
  Variant.class_eval do
    validates :cost_price, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 9999999 }, allow_nil: true
  
    def price_after_discount
      price - (price * discount/100)
    end
  end
end