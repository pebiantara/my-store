module Spree
  class Page < ActiveRecord::Base
    include TenantScoped
    validates :title, :content, presence: true
    validates :title, uniqueness: true
    before_validation :normalize_slug, on: :update

    extend FriendlyId   
    friendly_id :slug_candidates, use: :slugged

    translates :title, :content, :slug, :fallbacks_for_empty_translations => true
    include SpreeI18n::Translatable

    def slug_candidates
      [:title]
    end

    def to_param
      slug
    end

    private
      def normalize_slug
        self.slug = normalize_friendly_id(slug)
      end
  end
end