module Spree
  class Task
    attr_accessor :tenant_code

    def initialize(attributes={})
      self.tenant_code = attributes[:code] ? attributes[:code] : 'salihara' 
    end

    def wipe
      Multitenant.current_tenant = Spree::Tenant.find_by_code(self.tenant_code)
      #clear products
      Spree::Product.destroy_all
      Spree::Product.with_deleted.where(tenant_id: Multitenant.current_tenant).delete_all

      #clear orders
      Spree::Order.destroy_all
      #clear shipping method
      Spree::ShippingMethod.destroy_all
      #clear taxonomies
      Spree::Taxonomy.destroy_all
      #clear donation
      Spree::Donation.destroy_all
      #prototype
      Spree::Prototype.destroy_all
      #properties
      Spree::Property.destroy_all
      #shipping
      Spree::ShippingCategory.destroy_all
    end
  end
end