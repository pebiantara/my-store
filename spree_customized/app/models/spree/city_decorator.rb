module Spree
  class City < ActiveRecord::Base
    include TenantScoped
    validates :name, :code_name, presence: true
    validates_uniqueness_of :name, :scope => [:tenant_id, :state_id]

    belongs_to :country
    belongs_to :state
    has_many   :sub_districts, dependent: :destroy
    has_many :shippings, class_name: Spree::Jne::Shipping
    scope :indonesia, -> {where(iso: "ID")}
  end
end