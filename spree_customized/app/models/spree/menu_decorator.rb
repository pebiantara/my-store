#this models for manage dinamic menu for each tenant
require 'active_record'
module Spree
  class Menu < ActiveRecord::Base
    include TenantScoped

    validates :name, :key, presence: true
    #validates :key, :tenant_id, uniqueness: true
    validates :key, :uniqueness => {:scope => [:key, :tenant_id]}

    scope :menu_parents, -> { where(parent_id: nil) }
    scope :actived, -> { where active: true }

    has_many :childs_menu, class_name: "Menu", foreign_key: :parent_id
    belongs_to :parent_menu, class_name: "Menu", foreign_key: :parent_id, dependent: :destroy

    class << self    
      def generate_data
        Spree::Tenant.all.each do |tenant|
          self.generate_single_data(tenant)
        end
      end

      def generate_single_data(tenant)
        menus = self.default_data
        menus.each do |par, ch|
          menu_parent = Spree::Menu.where(key: par.to_s, tenant_id: tenant.id).first_or_initialize
          menu_parent.name = par.to_s
          if menu_parent.save
            puts "save '#{menu_parent.name}'"
            ch.each do |menu_child|
              childs = menu_parent.childs_menu.where(key: menu_child[:key], parent_id: menu_parent.id).first_or_initialize
              childs.name = menu_child[:name]
              childs.tenant_id = tenant.id
              childs.active = true
              childs.save
              puts "save '#{menu_parent.name}'->'#{childs.name}'"
            end
          end
        end
      end

      def default_data
        {
          orders: [
            {name: "payments",            key: "payments"},
            {name: "creditcard_payments", key: "creditcard_payments"},
            {name: "shipments",           key: "shipments"},
            {name: "credit_cards",        key: "credit_cards"}
            ],
          products: [
            {name: "products",     key: "sub_products"},
              {name: "option_types", key: "option_types"},
              {name: "properties",   key: "properties"},
              {name: "prototypes",   key: "prototypes"},
              {name: "taxonomies",   key: "taxonomies"},
              {name: "variants",     key: "variants"},
              {name: "taxons",       key: "taxons"}              
            ],
          reports: [],
          configurations: [
              {name: "mail_methods", key: "mail_methods"},
              {name: "tax_categories",       key: "tax_categories"},
              {name: "tax_rates",            key: "tax_rates"},
              {name: "tax_settings",         key: "tax_settings"},
              {name: "zones",                key: "zones"},
              {name: "countries",            key: "countries"},
              {name: "states",               key: "states"},
              {name: "cities",               key: "cities"},
              {name: "sub district",         key: "sub_districts"},
              {name: "payment methods",      key: "payment_methods"},
              {name: "taxonomies",           key: "config_taxonomies"},
              {name: "shipping methods",     key: "shipping_methods"},
              {name: "shipping_categories",  key: "shipping_categories"},
              {name: "stock_transfers",      key: "stock_transfers"},
              {name: "stock_locations",      key: "stock_locations"},
              {name: "analytics_trackers",   key: "trackers"}
            ],
          promotions: [],
          users: [],
          pages: [],
          reviews: [],
          donations: []
        }
      end
    end
  end
end