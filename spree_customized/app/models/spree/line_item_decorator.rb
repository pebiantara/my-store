module Spree
  LineItem.class_eval do
    def copy_price
      if variant
        self.price = variant.price - (variant.discount.to_f/100 * variant.price) #if price.nil?
        self.cost_price = variant.cost_price if cost_price.nil?
        self.currency = variant.currency if currency.nil?
      end
    end
  end
end