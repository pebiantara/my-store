Spree::AppConfiguration.class_eval do
  preference :enabled_frontend, :boolean, default: true
  preference :default_locale, :string, default: "id"
  preference :company_profile_site, :string, default: ""
  preference :store_address, :string, default: ""
  preference :user_confirm, :boolean, default: true
  preference :merchant_email, :string, default: ""
  preference :merchant_phone, :string, default: ""
  preference :slider, :boolean, default: ""
  preference :store_name, :string, default: "Nama Toko Anda"
  preference :template, :string, default: "andromeda"

  def self.templates
    %w(andromeda shopper darknight kool arround snow)
  end
end