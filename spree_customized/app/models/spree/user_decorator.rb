module Spree
  User.class_eval do
    has_many :product_reviews
    validate :no_attachment_errors
    has_many :bca_transactions, class_name: Spree::Bca::Transaction, foreign_key: :member_id
    belongs_to :payment_method, class_name: Spree::PaymentMethod

    has_attached_file :profile_picture,
                      styles: { mini: '48x48>', small: '100x100>', medium: '240x240>', large: '600x600>' },
                      default_style: :small,
                      url: '/spree/users/:id/:style/:basename.:extension',
                      path: ':rails_root/public/spree/users/:id/:style/:basename.:extension',
                      convert_options: { all: '-strip -auto-orient -colorspace sRGB' },
                      default_url: "/assets/user.jpg"

    validates_attachment_content_type :profile_picture, :content_type => /\Aimage\/.*\Z/

    devise :confirmable
    before_save :check_user_confirmable
    before_save :set_tenant

    roles_table_name = Role.table_name
    scope :customers, -> { joins("LEFT JOIN spree_roles_users ON spree_roles_users.user_id = spree_users.id LEFT JOIN spree_roles ON spree_roles.id = spree_roles_users.role_id") }

    def set_tenant
      self.tenant_id = Multitenant.current_tenant.try :id
    end

    class << self
      def current_user=(user)
        Thread.current[:current_user] = user
      end

      def current_user
        Thread.current[:current_user]
      end
    end

    def check_user_confirmable
      unless Spree::Config[:user_confirm]
        self.skip_confirmation!
      end
    end
	
    def check_permissions?(controller_name)
      !spree_roles.joins(:features).where("spree_features.controller_name = ?", 'spree/admin/products').empty?
    end
    
    def send_member_confirmation
      UserMailer.member_confirmation(self).deliver
    end

    def as_superadmin?
      as_superadmin
    end

    def confirmed?
      !confirmed_at.nil?
    end

    def check_payment
      transaction = bca_transactions.last
      !transaction.nil? && payment_method.is_a?(Spree::Bca::KlikPay) && !transaction.response_message.nil?
    end

    #for login
    def self.api_authenticate(email, password)
      user = User.find_for_authentication(:email => email)
      user = User.find_for_authentication(:username => email) unless user
      user and user.valid_password?(password) ? user : nil
    end

    def self.api_authenticate_for_tenant(tenant, email, password)
      user = User.api_authenticate(email, password)
      user = user and user.tenant == tenant ? user : nil
    end

    def no_attachment_errors
      unless profile_picture.errors.empty?
        # uncomment this to get rid of the less-than-useful interrim messages
        # errors.clear
        errors.add :profile_picture, "Paperclip returned errors for file '#{profile_picture_file_name}' - check ImageMagick installation or image source file."
        false
      end
    end

    def expired?
      return false if expired_at.blank?
      expired_at < Time.zone.now
    end

    def can_be_a_member?
      #(!confirmed? && bca_transactions.last.try(:status).nil?) || !expired_at.nil?
      expired_at.nil? || (expired_at && expired_at < Time.zone.now)
    end
  end
end