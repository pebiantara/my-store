module Spree
  Adjustment.class_eval do
    scope :donation, -> { where(source_type: 'Spree::Donation') }
  end
end