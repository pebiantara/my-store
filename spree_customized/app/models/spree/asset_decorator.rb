module Spree
  Asset.class_eval do
    before_validation :check_max_image, on: :create

    def check_max_image
      if viewable.is_a?(Spree::Variant)
        max_products_image = Multitenant.current_tenant.max_products_image.to_i
        current_images = viewable.product.images.count
        self.errors.add(:max_image, Spree.t('flash.errors.max_image_per_product')) if max_products_image < current_images + 1
      end
    end
  end
end
