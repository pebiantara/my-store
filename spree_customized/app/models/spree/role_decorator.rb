module Spree
  Spree::Role.class_eval do
    #has_and_belongs_to_many :features, :join_table => :spree_roles_features
    #include HistoryLog

    # validates :name, :tenant_id, :on => :create
    validates_uniqueness_of :name, :case_sensitive => false

    def self.search(params)
      return self.all if params.blank?
      search = params[:keyword]
      self.where('name ILIKE ?', "%#{search}%")
    end

  end
end