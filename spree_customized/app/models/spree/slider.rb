module Spree
  class Slider < ActiveRecord::Base
    include TenantScoped
    has_one  :slider_image, as: :viewable, dependent: :destroy, class_name: "Spree::Image"
    belongs_to :tenant
    belongs_to :product
    accepts_nested_attributes_for :slider_image

    validates :title, :slider_image, presence: true
  
    def have_url?
      link.present? || product.present?
    end

    def get_url
      link || "products/#{product.slug}"
    end
  end
end