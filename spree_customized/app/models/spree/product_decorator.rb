module Spree
  Product.class_eval do

    #friendly_id :slug_candidates, use: [:slugged, :globalize]
    delegate_belongs_to :master, :discount
    has_many :product_reviews, dependent: :destroy
    after_create :creating_qr_code
    before_validation :check_max_products, on: :create
    
    alias_method :reviews, :product_reviews

    after_save if: :slug_changed? do |apt|
      creating_qr_code
    end

    def discount?
      !discount.to_f.zero?
    end

    def price_after_discount
      new_price = price - (price * discount/100)
      Money.new new_price, currency: Spree::Config[:currency]
    end

    def check_max_products
      max_product = Multitenant.current_tenant.max_products.to_i
      current_products = Multitenant.current_tenant.products.count
      self.errors.add :max_products, Spree.t('flash.message.errors.cant_create_new_product') if max_product < current_products + 1
    end

    def creating_qr_code
      #write directory if not exist
      path   = "#{Rails.root}/public/spree/products"
      [tenant.code, "qrcodes"].each do |folder|
        path = [path, folder].join("/")
        Dir.mkdir path unless File.exist?(path)
      end
      text   = "#{Spree::Config[:site_url]}/products/#{self.slug}"
      qrcode = RQRCode::QRCode.new(text, :size => 10, :level => :h)
      #creating 3 version size
      [50, 1100, 200].each do |size|
        qrcode.to_img.resize(size, size).save([path, self.slug + "-#{size}.png"].join("/"))
      end
    end

    def review_text_count
      reviews.count.to_s + " review" + s_review.to_s
    end

    def s_review
      's' if reviews.count > 0
    end

    def self.best_seller
      joins(:variants_including_master).joins("INNER JOIN spree_line_items ON spree_variants.id = spree_line_items.variant_id").uniq.last(10)
    end

    def qrcode_url(size=50)
      "/spree/products/#{tenant.code}/qrcodes/#{self.slug}-#{size}.png"
    end

    def self.generate_qrcode
      self.all.each do |prod|
        prod.creating_qr_code
      end
    end
    
    def is_new?
      return false unless available_on
      available_on + 2.weeks >= Time.now
    end

    def is_sale?
      
    end

    def show_rates?
      rates > 0
    end

    def rates
      return 0 if reviews.empty?
      reviews.sum(:rating)/reviews.count
    end

    def slug_candidates
      [:name]
    end

    def event_days
      product_properties.joins(:property).where("lower(spree_properties.name) = ?", 'event days').first.try(:value)
    end

    def event_place
      product_properties.joins(:property).where("lower(spree_properties.name) = ?", 'event place').first.try(:value)
    end

    def event_time
      product_properties.joins(:property).where("lower(spree_properties.name) = ?", 'event time').first.try(:value)
    end

    def is_event?
      !taxons.where("lower(name) = ?", 'acara').first.nil?
    end
  end
end