module Spree
  class ProductReview < ActiveRecord::Base
    include TenantScoped

    validates :name, :review, :subject, :rating, presence: true
    belongs_to :product
    belongs_to :user

    scope :published, -> {where status: 'published'}
    scope :unpublish, -> {where status: 'unpublish'}
    scope :all_new, -> {where status: 'new'}

    def self.search(params)
      if params[:status]
        self.where(status: params[:status]).page(params[:page]).per(10).order("id DESC")
      else
        scoped.page(params[:page]).per(10).order("id DESC")
      end
    end

    def is_new?
      self.status.eql?("new")
    end
  end
end