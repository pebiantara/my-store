module Spree
  Taxon.class_eval do
    has_many :taxon_childs, class_name: "Spree::Taxon", foreign_key: :parent_id
  end
end