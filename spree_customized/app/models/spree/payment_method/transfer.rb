module Spree
  class PaymentMethod::Transfer < Spree::PaymentMethod
    validates :name, presence: true
    attr_accessor :post_url, :merchant_name, :klik_pay_code, :http_method, :payment_flag_url, :payment_inquiry_url,
    :clear_key, :pay_type, :success, :message

    preference :content, :text, default: '' 
      #PAYTYPE = {:"01" => "Full Transaction", :"02" => "Installment Transaction", :"03" => "Combination"}
      def authorize(amount, source, options_data={})
        source.success = true
        source
      end

      def cancel(response_code)
        if response_code
          #process cancel payment
        end
      end

      def payment_source_class
        Spree::Payment
      end

      def method_type
        'transfer'
      end

      def success?
        success
      end

      def authorization
        nil
      end

      def purchase(amount, source, hash_data)
        source.success = false
        source
      end

      def payment_profiles_supported?
        true
      end

      def provider
        integration_options = options
        @provider ||= provider_class.new(integration_options)
      end

      def options
        options_hash = {}
        preferences.each { |key, value| options_hash[key.to_sym] = value }
        options_hash
      end

      def provider_class
        self.class
      end
    end
  end