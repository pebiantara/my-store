module Spree
  Tenant.class_eval do
    has_many :spree_menus, class_name: "Spree::Menu"
    has_many :products
    has_many :users
    has_many :cities
    has_many :sub_districts
    has_many :states
    has_one  :tenant_configuration, dependent: :destroy
    has_one  :logo, as: :viewable, dependent: :destroy, class_name: "Spree::Image"


    delegate :expired_at, :type_registration, :payment_status, :status, :max_products, :max_products_image, to: :tenant_configuration

    accepts_nested_attributes_for :tenant_configuration
    accepts_nested_attributes_for :logo

    after_create :create_tenant_spree_data

    def expired?
      begin
        expired_at.to_date < Time.now.to_date if expired_at
      rescue
        false
      end
    end

    def is_active?
      begin
        status == 'active'
      rescue
        false
      end
    end

    def have_states?
      !self.states.first.nil?
    end

    def have_cities?
      !self.cities.first.nil?
    end

    def have_sub_districts?
      !self.sub_districts.first.nil?
    end

    def prepend_views
      path = "spree_customized/tenants/#{self.code}/views"

      #remove othe tenant views
      Rails.application.config.paths['app/views'] = Rails.application.config.paths['app/views'].reject { |registered_path| registered_path.include?'tenants' and registered_path != path}

      ActionController::Base.prepend_view_path(path)
      Rails.application.paths['app/views'] << path unless Rails.application.paths['app/views'].include? path
    end

    def active_source
      #demoduling source per tenant
      tenants = Spree::Tenant.where("code != ?", self.code)
      tenants.each do |tenant|
        if Object.const_defined?(self.code.titleize)
          ActiveSupport::Dependencies.remove_constant self.code.titleize.constantize
        end
      end

      sources = [Dir.glob(File.join(Rails.root, "/spree_bca/app/**/*.rb")), Dir.glob(File.join(Rails.root, "/spree_customized/app/**/*.rb"))]

      sources.flatten.each do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

      sources = Dir.glob(File.join(Rails.root, "/spree_customized/tenants/#{self.code}/**/**/*.rb"))

      sources.flatten.each do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end

    def create_tenant_spree_data
      #Spree::Role.create([, {name: 'user', tenant_id: self.id}])
      Spree::Country.where({iso_name: "ID", name: "INDONESIA", numcode: "ID", states_required: false, tenant_id: self.id}).first_or_initialize.save

      #update user
      user = Spree::User.where(tenant_id: self.id).first_or_initialize
      user.email = "admin@#{self.code}.com"
      user.password = '12345678'
      user.password = '12345678'
      user.as_superadmin = true
      user.skip_confirmation!

      %w(user admin).each do |role|
        Spree::Role.where(tenant_id: self.id, name: role).first_or_initialize.save validate: false
      end
      user.spree_roles.destroy_all
      user.spree_role_ids = Spree::Role.where(tenant_id: self.id).collect &:id
      # user.spree_roles.build name: 'admin', tenant_id: self.id
      # user.spree_roles.build name: 'user', tenant_id: self.id
      user.save
      puts user.errors.inspect

      unless Spree::Preference.where(tenant_id: self.id).any?
        #setup default preferences
        Spree::Preference.create(self.preferences_default) 
        #setup structure folder for tenant
        creating_tenant_source

        #setup default shipping
        setup_shipping

        #setup menu
        Spree::Menu.generate_single_data(self)
      end
    end

    def setup_shipping
      shipping_method = Spree::ShippingMethod.new({name: "JNE", display_on: "", tracking_url: "", admin_name: "JNE", tenant_id: self.id})
      shipping_method.shipping_categories.build name: "DEFAULT", tenant_id: self.id
      shipping_method.build_calculator type: "Spree::Calculator::Shipping::FlatRate", tenant_id: self.id
      shipping_method.save

      ActiveRecord::Base.connection.execute "INSERT INTO spree_zones (name, description, default_tax, tenant_id) values ('Asia', 'Asia', 't', #{self.id})"
      zone = Spree::Zone.last
      ActiveRecord::Base.connection.execute "INSERT INTO spree_shipping_methods_zones (shipping_method_id, zone_id) values (#{shipping_method.id}, #{zone.id})"
    end

    def creating_tenant_source
      %w(controllers models).each do |fol|
        tenant_folder = File.join Rails.root.to_s, 'spree_customized/tenants', code, fol
        FileUtils.mkdir_p tenant_folder unless File.exist? tenant_folder
      end
    end

    def preferences_default
      [
        {:tenant_id=>self.id, :value=>"5.0", :key=>"spree/calculator/shipping/flat_rate/amount/1", :value_type=>"decimal"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/calculator/shipping/flat_rate/currency/1", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"5.0", :key=>"spree/calculator/shipping/flat_rate/amount/4", :value_type=>"decimal"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/calculator/shipping/flat_rate/currency/4", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"15.0", :key=>"spree/calculator/shipping/flat_rate/amount/3", :value_type=>"decimal"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/calculator/shipping/flat_rate/currency/3", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"10.0", :key=>"spree/calculator/shipping/flat_rate/amount/2", :value_type=>"decimal"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/calculator/shipping/flat_rate/currency/2", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"8.0", :key=>"spree/calculator/shipping/flat_rate/amount/5", :value_type=>"decimal"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/calculator/shipping/flat_rate/currency/5", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"true", :key=>"spree/app_configuration/check_for_spree_alerts", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"#{self.code}", :key=>"spree/app_configuration/site_name", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"", :key=>"spree/app_configuration/default_seo_title", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"#{self.code}", :key=>"spree/app_configuration/default_meta_keywords", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"#{self.code} site", :key=>"spree/app_configuration/default_meta_description", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"#{self.domain}", :key=>"spree/app_configuration/site_url", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"true", :key=>"spree/app_configuration/allow_ssl_in_production", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"false", :key=>"spree/app_configuration/display_currency", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"false", :key=>"spree/app_configuration/allow_ssl_in_staging", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"false", :key=>"spree/app_configuration/allow_ssl_in_development_and_test", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"false", :key=>"spree/app_configuration/hide_cents", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"IDR", :key=>"spree/app_configuration/currency", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"before", :key=>"spree/app_configuration/currency_symbol_position", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>".", :key=>"spree/app_configuration/currency_decimal_mark", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>",", :key=>"spree/app_configuration/currency_thousands_separator", :value_type=>"string"},
        {:tenant_id=>self.id, :value=>"true", :key=>"spree/app_configuration/enabled_frontend", :value_type=>"boolean"},
        {:tenant_id=>self.id, :value=>"", :key=>"spree/app_configuration/default_country_id", :value_type=>"integer"}
      ]
    end
  end
end