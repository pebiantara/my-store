module Spree
  Country.class_eval do
    has_many :cities
    scope :ordered, -> {order(:name)}
  end
end