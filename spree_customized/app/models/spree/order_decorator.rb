module Spree
  Order.class_eval do
    has_one :donation, dependent: :destroy

    checkout_flow do
      go_to_state :address, if: ->(order) {!order.only_ticket?}
      go_to_state :delivery
      go_to_state :payment, if: ->(order) do
        order.set_shipments_cost if order.shipments.any?
        order.payment_required?
      end
      go_to_state :confirm, if: ->(order) { order.confirmation_required? }
      go_to_state :complete
      remove_transition from: :delivery, to: :confirm
    end

    def only_ticket?
      stats = []
      line_items.each do |item|
        stats << item.variant.product.is_event?
      end
      stats.uniq.include?(true) && stats.uniq.length.eql?(1)
    end

    def have_ticket?
     stats = []
      line_items.each do |item|
        stats << item.variant.product.is_event?
      end
      stats.uniq.include?(true) && !stats.uniq.length.eql?(1) 
    end

    def check_payment_order
      valid_payment = payments.last
      source = valid_payment.try(:source)
      !source.nil? && valid_payment.payment_method.is_a?(Spree::Bca::KlikPay) && source.response_message.present?
    end

    def allow_cancel?
      return false unless completed? and state != 'canceled'
      return false if payment_state == 'paid' && shipment_state != 'backorder'
      shipment_state.nil? || %w{ready backorder pending}.include?(shipment_state)
    end

    def products_have_event?
      have_event = []
      line_items.each do |item|
        have_event << item.product.is_event?
      end
      have_event.uniq.include?(true)
    end

    def approve!
      payment_valid = payments.valid.last
      if payment_valid && payment_valid.source.is_a?(Spree::PaymentMethod::Transfer)
        payment_valid.update_attributes state: 'completed'
        self.update_attributes payment_state: 'paid'
      end
      shipments.last.ready! rescue nil
      update_column(:considered_risky, false)
    end


    def empty!
      line_items.destroy_all
      updater.update_item_count
      donation.destroy if donation

      adjustments.destroy_all
      update_totals
      persist_totals
    end

    private
    def update_params_payment_source
      if has_checkout_step?("payment") && self.payment?
        if @updating_params[:payment_source].present?
          source_params = @updating_params.delete(:payment_source)[@updating_params[:order][:payments_attributes].first[:payment_method_id].underscore]
          if source_params
            @updating_params[:order][:payments_attributes].first.delete_if{|x,v| ['source_id', 'source_type'].include?(x)}
            @updating_params[:order][:payments_attributes].first[:source_attributes] = source_params
          end
        end

        if (@updating_params[:order][:payments_attributes])
          @updating_params[:order][:payments_attributes].first[:amount] = self.total
        end
      end
    end
  end
end