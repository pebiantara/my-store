module Spree
  class Social < ActiveRecord::Base
    include TenantScoped

    validates :provider, :url, presence: true

    def self.providers
      [:facebook, :twitter, :google_plus, :instagram, :pinterest, :flickr, :youtube]
    end

    def self.dummy
      providers.each do |provider|
        new(provider: provider, url: "http://#{provider}.com").save
      end
    end
  end
end