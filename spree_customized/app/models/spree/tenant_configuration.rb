module Spree
  class TenantConfiguration < ActiveRecord::Base
    belongs_to :tenant
    before_save :set_expired_at

    def set_expired_at
      self.expired_at = Time.zone.now + 1.month unless self.expired_at
    end
  end
end