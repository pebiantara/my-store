module Spree
  class Donation < ActiveRecord::Base
    include TenantScoped
    belongs_to :order
    belongs_to :user
    belongs_to :payment_method, class_name: Spree::PaymentMethod

    has_many :transactions, class_name: Spree::Bca::Transaction
    has_many :adjustments, as: :source, dependent: :destroy

    validates :total, :numericality => {greater_than_equal: 10000, less_than_or_equal_to: 9999999}
    validates :email, :name, presence: true, if: :order_or_user_blank?
 
    before_save :set_identifier
    after_save :set_bca_transaction

    attr_accessor :anonim

    def set_identifier
      self.identifier = Time.zone.now.to_i
    end

    def check_payment
      transaction = transactions.last
      !transaction.nil? && payment_method.is_a?(Spree::Bca::KlikPay) && !transaction.response_message.nil?
    end

    def compute_amount(adjustable)
      self.total.to_f
    end

    def order_or_user_blank?
      (self.order_id.blank? && !anonim.present?) || !anonim.present?
    end

    def set_bca_transaction
      transaction = Spree::Bca::Transaction.where(transaction_no: "#{identifier}", donation_id: self.id, tenant_id: Multitenant.current_tenant.id).first_or_initialize
      transaction.save
    end
  end
end