module Spree
  module Jne
    class Shipping < ActiveRecord::Base
      require 'roo'

      self.table_name = "spree_jne_shippings"
      include TenantScoped
      has_one :price, class_name: Spree::Jne::Price, dependent: :destroy
      before_save :set_tenant

      belongs_to :city
      belongs_to :sub_district

      delegate :name, to: :city, prefix: 'city'
      delegate :name, to: :sub_district, prefix: 'district'
      delegate_belongs_to :price, :reg_price, :reg_estimation, :oke_price, :oke_estimation, :yes_price, :yes_estimation

      scope :yes, -> {where(jne_type: 'yes')}
      scope :ss, -> {where(jne_type: 'oke')}
      scope :reg, -> {where(jne_type: 'reg')}

      accepts_nested_attributes_for :price
      
      def self.jne_type
        [["Express", "yes"], ["OKE", "oke"], ["Reguler", "reg"]]
      end

      def self.search(params)
        conditions = []
        
        conditions << "city_id = '#{params[:ct]}'" if params[:ct].present?

        if conditions.any?
          where(conditions.join(" AND "))
        else
          scoped
        end
      end

      def set_tenant
        self.tenant_id = tenant.id if tenant
      end

      def self.upload_file(file, jtype)
        file_data = process_upload(file)
        return {error: true, message: file_data[:message]} if file_data[:error]
        #path = "#{Rails.root}/public/spree/sample_shippings/jne.xls"
        path = file_data[:path]
        delay(queue: 'shippings').open_spreadsheet(path, Multitenant.current_tenant.id)
        {error: false, message: "Successfully to upload, data will importing in the background"}
      end

      def self.process_upload(file)
        return {error: true, message: "Please select file."} if file.nil?
        if [".xls", ".xlsx"].include?(File.extname(file.original_filename))
          uploading = save_file file
          #prosess upload
          {error: false, message: 'Successfully Upload, data will importing in the background'}.merge(uploading)
        else
          {error: true, message: "Invalid Format Type of file."}
        end
      end

      def self.get_price(package, jtype)
        address = package.contents.first.line_item.order.ship_address rescue nil
        if address        
          #find level district
          jne = joins(:price).where(city_id: address.city_id, sub_district_id: address.sub_district_id).first 
          return eval "jne.#{jtype}_price" if jne

          #find level district
          jne = joins(:price).where(sub_district_id: address.sub_district_id).first 
          return eval "jne.#{jtype}_price" if jne

          #find level city
          jne = joins(:price).where(city_id: address.city_id).first 
          return eval "jne.#{jtype}_price" if jne
        end
      end

      def self.open_spreadsheet(path, tenant_id=nil)
        spreadsheet = case File.extname(path)
          when ".xls" then Roo::Excel.new(path, nil, :ignore)
          when ".xlsx" then Roo::Excelx.new(path, nil, :ignore)
        end
        header = spreadsheet.row(1)
        state_name = ""
        city_name  = ""
        hash = []
        tenant = tenant_id
        country = Spree::Country.where("tenant_id = ? and (lower(iso) =? or lower(iso_name) =?)", tenant_id, "id", "id").first
        header = ["Kota / Kabupaten", "Kecamatan", "REGULER", nil, "OKE", nil, "YES"]
        Spree::Jne::Shipping.where(tenant_id: tenant).delete_all
        Spree::Jne::Price.where(tenant_id: tenant).delete_all
        if header != spreadsheet.first
          return false
        end  
        spreadsheet.each_with_index do |row, index|
          next if index < 2
          city_name  = row[0] if row[0].present?
          district_name  = row[1] if row[1].present?
          reg_price      = row[2] if row[2].present?
          reg_estimation = row[3] if row[3].present?
          oke_price = row[4] if row[4].present?
          oke_estimation = row[5] if row[5].present?
          yes_price = row[6] if row[6].present?
          yes_estimation = '1'
          
          city = Spree::City.where(country_id: country.id, name: city_name, code_name: city_name, tenant_id: tenant_id).first_or_initialize
          district = city.sub_districts.where(name: district_name, code_name: district_name, tenant_id: tenant_id).first_or_initialize
          if city.save && district.save
            shippings = Spree::Jne::Shipping.new()
            hash << {city_id: city.id, sub_district_id: district.id, tenant_id: tenant, price_attributes: {reg_price: reg_price, reg_estimation: reg_estimation, oke_price: oke_price, oke_estimation: oke_estimation, yes_price: yes_price, yes_estimation: yes_estimation, tenant_id: tenant}}
          end
        end
        Spree::Jne::Shipping.create hash
        #File.delete(path) if File.exist?(path)
      end

      def self.save_file upload
        file_name = upload.original_filename if upload != ''
        file = upload.read    

        file_type = file_name.split('.').last
        new_name_file = "shippings-#{Time.now.to_i}"
        new_file_name_with_type = "#{new_name_file}." + file_type
        upload_path = "#{Rails.root.to_s}/public/spree/shippings"

        #create directory if path not found
        unless Dir.exists?(upload_path)
          dirs = upload_path.split("/")
          tmp_dir = []
          dirs.each do |dir|
            tmp_dir << dir
            next if tmp_dir.join("/") == ""
            path = tmp_dir.join("/")
            puts path.inspect
            unless Dir.exists?(path)
              Dir.mkdir(path)
            end
          end
        end
        path_of_filename = upload_path + "/" + new_file_name_with_type
        File.open(path_of_filename, "wb")  do |f|  
          f.write(file) 
        end

        {path: path_of_filename, name: new_file_name_with_type}
      end

      def self.collecting_data
        tenant = Spree::Tenant.find_by_code("salihara")
        country = Spree::Country.where(name: "Indonesia", tenant_id: tenant.id).first
        shippings =  Spree::Jne::Shipping.where(tenant_id: tenant.id).order(:state_name).group_by{|x| x.state_name}
        state = {}
        shippings.each do |key, val|
          state["#{key}"] = {}
          val.group_by{|x| x.city_name}.each do |k, v|
            state["#{key}"]["#{k}"] = {}
            state["#{key}"]["#{k}"] = v.group_by{|x| x.district_name}.map{|x| x.first}
          end
        end
        Spree::State.where(country_id: country.id, tenant_id: tenant.id).destroy_all
        state.each do |key, val|
          state = Spree::State.new(name: key, tenant_id: tenant.id, country_id: country.id)
          state.save
          val.each do |city, districts|
            city = state.cities.new(tenant_id: tenant.id, country_id: country.id, name: city, code_name: city)
            city.save
            districts.each do |district|
              dis = city.sub_districts.build(tenant_id: tenant.id, name: district, code_name: district)
              dis.save
            end
          end
        end
        puts "done"
      end
    end
  end
end