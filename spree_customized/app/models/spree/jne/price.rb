module Spree
  module Jne
    class Price < ActiveRecord::Base
      require 'roo'

      self.table_name = "spree_jne_prices"
      belongs_to :shipping, class_name: Spree::Jne::Shipping
      include TenantScoped

    end
  end
end