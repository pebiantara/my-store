module Spree
  Spree::Price.class_eval do
    validates :amount, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 9999999 }, allow_nil: true
  end
end