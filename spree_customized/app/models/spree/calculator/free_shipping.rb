module Spree
  class Calculator::FreeShipping < Calculator
    def self.description
      Spree.t(:free_shipping)
    end

    def compute(object)
      0.0
    end
  end
end