Spree::Calculator::Shipping::PerItem.class_eval do
  def compute_package(package)
    if calculable.use_jne?
      compute_package_jne(package, calculable.jne_type)      
    else
      content_items = package.contents
      self.preferred_amount * content_items.sum { |item| item.quantity }
    end
  end

  def compute_package_jne(package, type_package)
    jne_price = Spree::Jne::Shipping.get_price(package, type_package)
    price = jne_price ? jne_price : self.preferred_amount

    content_items  = package.contents
    total = 0
    content_items.each_with_index do |content_item, index|
      item   = content_item.variant if content_item.variant
      weight = item ? item.weight.to_f : 0.0
      width  = item ? item.width.to_f : 0.0
      height = item ? item.height.to_f : 0.0
      depth  = item ? item.depth.to_f : 0.0
      total += price * [weight, (width * height * depth / 6000)].max * content_item.quantity
    end
    total
    #self.preferred_amount * content_items.sum { |item| item.quantity }
  end
end