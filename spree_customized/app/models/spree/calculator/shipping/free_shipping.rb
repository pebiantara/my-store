require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class FreeShipping < Calculator
      def self.description
        Spree.t(:free_shipping)
      end

      def compute(object)
        if object.is_a?(Array)
          return if object.empty?
          order = object.first.order
        else
          order = object
        end

        #order.ship_total
        0.0
      end
    end
  end
end