module Spree
  class Feature < ActiveRecord::Base
    include TenantScoped
    has_and_belongs_to_many :roles, :join_table => :spree_roles_features
    # validates :name, :tenant_id, :on => :create
    validates_uniqueness_of :name, :case_sensitive => false

    def self.search(params)
      return self.all if params.blank?
      search = params[:keyword]
      self.where('name ILIKE ?', "%#{search}%")
    end

    def self.default_data
       Spree::Feature.delete_all
       Spree::Feature.create([
          {id: 1, name: "Management Product ",  tenant_id: "3",  controller_name: "spree/admin/products" },
          {id: 2, name: "Management Customer User",  tenant_id: "3",  controller_name: "spree/admin/users_customers"},
          {id: 3, name: "Management Admin User",  tenant_id: "3",  controller_name: "spree/admin/users"},
          {id: 4, name: "Management User Group",  tenant_id: "3",  controller_name: "spree/admin/roles" },
          {id: 5, name: "Management Transaction",  tenant_id: "3",  controller_name: "spree/admin/orders" },
          {id: 6, name: "Transaction Report",  tenant_id: "3",  controller_name: "spree/admin/reports"},
          {id: 7, name: "Customer Report",  tenant_id: "3",  controller_name: "spree/admin/reports_customers"},
          {id: 8, name: "Activity Log",  tenant_id: "3",  controller_name: "spree/admin/activity_logs" }
       ])
    end
  end
end