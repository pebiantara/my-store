module Spree
  class PromoBanner < ActiveRecord::Base
    include TenantScoped
    has_one  :promo_banner_image, as: :viewable, dependent: :destroy, class_name: "Spree::Image"
    belongs_to :tenant
    belongs_to :product
    accepts_nested_attributes_for :promo_banner_image

    validates :type_banner, :promo_banner_image, presence: true
  
    def have_url?
      url.present? || product.present?
    end

    def get_url
      url || "products/#{product.slug}"
    end

    def self.types
      [["Box1", "box1"], ["Box2", "box2"]]
    end
  end
end