module Spree
  class SubDistrict < ActiveRecord::Base
    include TenantScoped
    validates :name, :code_name, presence: true
    validates_uniqueness_of :name, :scope => [:tenant_id, :city_id]
    
    belongs_to :city
    has_many :shippings, class_name: Spree::Jne::Shipping
  end
end