module Spree
  class ActivityLog < ActiveRecord::Base
    belongs_to :user, foreign_key: :edit_by
    belongs_to :source, polymorphic: true

    def old_attributes
      JSON.parse(super)
    end

  end
end