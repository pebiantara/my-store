module Spree
  module Admin
    class ProductPropertiesController < ResourceController
      belongs_to 'spree/product', :find_by => :slug
      before_filter :find_properties
      before_filter :setup_property, :only => [:index]

      private
        def find_properties
          @properties = Spree::Property.pluck(:name)
        end

        def setup_property
          if @product.blank?
            @product = Spree::Product.find_by_slug(params[:product_id])
          end
          @product.product_properties.build
        end
    end
  end
end
