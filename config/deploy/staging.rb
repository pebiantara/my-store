require "rvm/capistrano"

set :user, "arip"
set :use_sudo, false
set :domain, "arip@202.138.229.148"
role :web, domain
role :app, domain
role :db,  domain, :primary => true

set :deploy_to, "/home/arip/project/spreecommerce"
set :repository,  "ssh://git@gitlab.kiranatama.net:222/Ari_Prasetya/spreecommerce.git"
set :branch, "staging"
set :keep_releases, 2
task :symlink_rvm do
 run "ln -nsf #{shared_path}/.rvmrc #{release_path}/.rvmrc"
end

task :bundle_install do
  run "cd #{release_path} && rvm use ruby-2.1.2@spreecommerce do bundle install"
end

task :precompile_asset do
  from = source.next_revision(current_revision)
  if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
    run "cd #{release_path} && rvm use ruby-2.1.2@spreecommerce do bundle exec rake assets:precompile --trace"
  else
    logger.info "Skipping asset pre-compilation because there were no asset changes"
  end
end

task :restart_server do
  run "cd #{current_path} && ./bin/thin"
end

task :migrate_server do
  run "cd #{current_path} && rvm use ruby-2.1.2@spreecommerce do bundle exec rake db:migrate RAILS_ENV=staging"
end
task :restart_delayed_job do
  run "cd #{current_path} && rvm use ruby-2.1.2@spreecommerce do bundle exec bin/delayed_job --queue=shippings stop RAILS_ENV=staging"
  run "cd #{current_path} && rvm use ruby-2.1.2@spreecommerce do bundle exec bin/delayed_job --queue=shippings start RAILS_ENV=staging"
end

after :deploy, "deploy:cleanup"
after :deploy, :symlink_rvm
after :deploy, "rvm:trust_rvmrc"
after :deploy, :bundle_install
#after :deploy, :precompile_asset
after :deploy, :migrate_server
after :deploy, :restart_server
after :deploy, :restart_delayed_job

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{current_path}"
  end
end
# set :delayed_job_command, "bin/delayed_job"
# after :deploy, "delayed_job:stop"
# after :deploy, "delayed_job:start"
