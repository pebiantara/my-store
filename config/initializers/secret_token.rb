# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Multitenant::Application.config.secret_key_base = '751184e7251e350a7e69223f219891aecc1e3cbd9c8dfbe2a793689eefc64a60ecb63dc8d8364d36c7cd7fca50a0a347bd763f6342d65d3d5941ccb365f7cbbc'
