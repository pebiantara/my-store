require 'capistrano/ext/multistage'

set :application, "spreecommerce"
set :stages, %w(staging production)
set :default_stage, 'staging'
set :scm, :git
set :deploy_via, :remote_cache

set :app_symlinks, %w{}
namespace :symlinks do
  desc "Link up our database.yml file in our shared config directory"
  task :symlink_database_yml, :roles => [:app, :web] do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  task :symlink_log, :roles => [:app, :web] do
    run "ln -nfs #{shared_path}/log #{release_path}/log"
  end
  task :symlink_tmp, :roles => [:app, :web] do
    run "rm -rf #{release_path}/tmp && ln -nfs #{shared_path}/tmp #{release_path}/tmp"
  end
  task :symlink_db_schema, :roles => [:app, :web] do
    run "ln -nfs #{shared_path}/schema.rb #{release_path}/db/schema.rb"
  end
  task :symlink_gemfile_lock, :roles => [:app, :web] do
    run "ln -nfs #{shared_path}/Gemfile.lock #{release_path}/Gemfile.lock"
  end
  task :symlink_uploads, :roles => [:app, :web] do
    run "ln -nfs #{shared_path}/uploads/spree #{release_path}/public"
  end
  task :symlink_compiled_assets, :roles => [:app, :web] do
    run "cd #{release_path} && bundle exec rake assets:clean && ln -nfs #{shared_path}/public/assets #{release_path}/public/assets"
  end
end

after  'deploy:update_code', 'symlinks:symlink_database_yml'
after  'deploy:update_code', 'symlinks:symlink_log'
after  'deploy:update_code', 'symlinks:symlink_tmp'
after  'deploy:update_code', 'symlinks:symlink_db_schema'
after  'deploy:update_code', 'symlinks:symlink_gemfile_lock'
after  'deploy:update_code', 'symlinks:symlink_uploads'
#after  'deploy:update_code', 'symlinks:symlink_compiled_assets'
