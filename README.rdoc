Sprecommerce for multitenant


Things you may want to cover:

* Ruby version
  ruby-2.1.2

* Database
  psql (PostgreSQL) 9.3.5


Installation step
-------------------------

1. Move to development branch
  $ git checkout development

2. Install required gem and dependencies based on Gemfile.lock
  $ bundle install

3. Database setup
  $ rake db:setup

4. Generate Menu
  $ rake menu:generate

5. Run server
  $ rails s


Step2 Create new tenant
----------------------------------------

1. Create tenant and add default admin
  $ bundle exec rake spree_multi_tenant:create_tenant domain=tokobaju.local code=tokobaju

2. For local development, add to etc/hosts
  127.0.0.1 tokobaju.local

3. Access the site
  http://tokobaju.local:3000


Add new admin
------------------
  $ rake spree_auth:admin:create



Deployment Step
------------------
deployment using capistrano and branch staging

1. Merge developement to staging
  $ git checkout staging
  $ git merge development
  $ git push origin staging

2. Run deploy step
  $ cap deploy