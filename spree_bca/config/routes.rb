Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  namespace :bca do
    resources :payments do
      collection do
        get :redirect_forward
        get :set_payment_timeout
      end
    end
  end

  Spree::PaymentMethod.load_routes
end