class AddCardNumberToCreditCards < ActiveRecord::Migration
  def change
    add_column :spree_credit_cards, :card_number, :string
  end
end
