xml = Builder::XmlMarkup.new
xml.instruct!
xml.OutputPaymentIPAY do
  xml.klikPayCode @options[:klikPayCode] unless @options.nil?
  xml.transactionNo @options[:transactionNo] unless @options.nil?
  xml.transactionDate @options[:transactionDate] unless @options.nil?
  xml.currency @options[:currency] unless @options.nil?
  xml.totalAmount @options[:totalAmount] unless @options.nil?
  xml.payType @options[:payType] unless @options.nil?
  if @options && @options[:approvalCode].present?
    xml.approvalCode do
      xml.fullTransaction @options[:approvalCode][:fullTransaction] if @options && @options[:approvalCode][:fullTransaction]
      xml.installmentTransaction @options[:approvalCode][:installmentTransaction] if @options && @options[:approvalCode][:installmentTransaction]
    end
  end
  xml.status @error[:status]
  xml.reason do
    xml.indonesian @error[:message][:id]
    xml.english @error[:message][:en]
  end
  xml.addtionalData ""
end