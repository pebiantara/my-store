xml = Builder::XmlMarkup.new
xml.instruct!
xml.OutputListTransactionIPAY do
  xml.klikPayCode @payment.try(:klik_pay_code)
  xml.transactionNo @payment.try(:transaction_no)
  xml.currency Spree::Config[:currency]
  xml.fullTransaction do
    xml.amount @order.try(:total)
    xml.description @donation ? "Donation #{@donation.name}" : "Payment for order #{@order.try(:number)}"
  end

  @line_items.each do |item|
    xml.installmentTransaction do
      xml.itemName item.variant.name
      xml.quantity item.quantity
      xml.amount item.price.to_f
      xml.tenor @payment_method.try(:tenor)
      xml.codePlan "10"
      xml.merchantId "12322"
    end
  end

  if @order.shipments.any?
    @order.shipments.each do |shipment|
      xml.installmentTransaction do
        xml.itemName Spree.t('shipment')
        xml.quantity 1
        xml.amount shipment.cost.to_f
        xml.tenor @payment_method.try(:tenor)
        xml.codePlan "10"
        xml.merchantId "12322"
      end
    end
  end

  if @order.adjustments.any?
    @order.adjustments.each do |adjustment|
      xml.installmentTransaction do
        xml.itemName adjustment.label
        xml.quantity 1
        xml.amount adjustment.amount
        xml.tenor @payment_method.try(:tenor)
        xml.codePlan "10"
        xml.merchantId "12322"
      end
    end
  end

  xml.miscFee ""
  xml.addtionalData ""
end