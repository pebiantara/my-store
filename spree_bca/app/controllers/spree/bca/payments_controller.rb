module Spree::Bca
  class PaymentsController < Spree::StoreController
    include Spree::Bca::PaymentHelper
    skip_before_filter :verify_authenticity_token
    before_filter :load_order_with_lock, only: :processing
    helper 'spree/orders'
   
    def set_payment_timeout
      tran = Spree::Bca::Transaction.where(transaction_no: params[:transaction_no]).last
      status = tran.update_attributes(payment_time_out: Time.zone.now + 30.seconds) rescue false
      render json: {status: status}
    end

    def klikpay_inquiry
      process_inquiry_path
    end

    def klikpay_return
      process_return_path
    end

    def process_return_path
      if request.post?
        respond_to do |format|
          if request.format.html?
            format.html {
              process_return
              render layout: false
            }
          else
            format.xml {
              collecting_for_xml
            }
          end
        end
      else
        redirect_to checkout_state_path("payment")
      end
    end

    def process_inquiry_path
      @transaction = Spree::Bca::Transaction.where(transaction_no: params[:transactionNo]).first
      if params[:donation_id].present? || @transaction.try(:donation_id)
        klikpay_inquiry_donation
      elsif params[:member].present? || @transaction.try(:member_id)
        klikpay_inquiry_member
      else
        klikpay_inquiry_order
      end
    end

    def klikpay_inquiry_order
      respond_to do |format|
        if request.format.html?
          format.html {
            load_order_with_lock
            unless @order
              #not_found and return
            end
            set_processing
            render :processing
          }
        else
          format.xml{
            load_order_with_lock
            unless @order
              #not_found and return
            end
            @payment = Spree::Bca::Transaction.where(klik_pay_code: params[:klikPayCode], transaction_no: params[:transactionNo]).first
            @order = @payment.order if @payment
            @line_items = @order.line_items if @order
            @payment_method = @order.payments.last.try(:payment_method).try(:options) if @order
          }
        end
      end
    end

    def klikpay_inquiry_donation
      respond_to do |format|
        if request.format.html?
          format.html {
            set_processing_donation
            render :processing_donation
          }
        else
          format.xml{
            @payment = Spree::Bca::Transaction.where(klik_pay_code: params[:klikPayCode], transaction_no: params[:transactionNo]).first
            @donation = @payment.donation if @payment
            @payment_method = @order.payments.last.try(:payment_method).try(:options) if @order
          }
        end
      end
    end

    def klikpay_inquiry_member
      respond_to do |format|
        if request.format.html?
          format.html {
            set_processing_member
            render :processing_member
          }
        else
          format.xml{
            @payment = Spree::Bca::Transaction.where(klik_pay_code: params[:klikPayCode], transaction_no: params[:transactionNo]).first
            @member  = @payment.member if @payment
            @payment_method = @member.payment_method.try(:options) if @member
          }
        end
      end      
    end

    def process_return
      @transaction_payment = Spree::Bca::Transaction.where(transaction_no: params["merchantTransactionID"]).last
      if @transaction_payment.try(:donation)
        collecting_for_html_donation
      elsif @transaction_payment.try(:member)
        collecting_for_html_member
      else
        load_order_with_lock
        collecting_for_html
      end
    end

    def collecting_for_html_member
      #status true if scrubCode not present or blank
      success_statuses = ["PENDING", "APPROVED"]
      statuses = [success_statuses, "DECLINED", "SCRUBBED", "ERROR", "CANCELLED"].flatten
      success = params["scrubCode"].blank?
      transaction_status = success_statuses.include?(params["transactionStatus"])
      @payment = Spree::Bca::Transaction.where(transaction_no: params["merchantTransactionID"]).last
      
      if success
        member   = @payment.try(:member)
        update_payment_transaction(@payment) if @payment
        member.update_attributes(payment_status: 'paid', expired_at: Time.zone.now + 1.year) if member
        member.send_member_confirmation
      end
    end


    def collecting_for_html_donation
      #status true if scrubCode not present or blank
      success_statuses = ["PENDING", "APPROVED"]
      statuses = [success_statuses, "DECLINED", "SCRUBBED", "ERROR", "CANCELLED"].flatten
      success = params["scrubCode"].blank?
      transaction_status = success_statuses.include?(params["transactionStatus"])
      @payment = Spree::Bca::Transaction.where(transaction_no: params["merchantTransactionID"]).last
      
      if success
        donation   = @payment.try(:donation)
        update_payment_transaction(@payment) if @payment
        donation.update_attributes(status: params["transactionStatus"]) if donation
      end
    end

    def update_payment_transaction(payment)
      payment.transaction_description = params["transactionDescription"]
      payment.user_define_value = params["userDefineValue"]
      payment.transaction_id = params["transactionID"]
      payment.transaction_type = params["transactionType"]
      payment.status = params["transactionStatus"]
      payment.card_no = params["cardNo"]
      payment.acquirer_code = params["acquirerCode"]
      payment.acquirer_merchant_account = params["acquirerMerchantAccount"]
      payment.acquirer_response_code = params["acquirerResponseCode"]
      payment.acquirer_approval_code = params["acquirerApprovalCode"]
      payment.scrub_code = params["scrubMessage"]
      payment.scrub_message = params["scrubMessage"]
      payment.save
    end

    def payment_state
      if params[:transactionStatus] == "PENDING"
        "pending"
      elsif params[:transactionStatus] == "APPROVED"
        "completed"
      else
        "failed"
      end
    end

    def collecting_for_html
      #status true if scrubCode not present or blank
      success_statuses = ["PENDING", "APPROVED"]
      statuses = [success_statuses, "DECLINED", "SCRUBBED", "ERROR", "CANCELLED"].flatten
      success = params["scrubCode"].blank?
      transaction_status = success_statuses.include?(params["transactionStatus"])
      @payment = Spree::Bca::Transaction.where(transaction_no: params["merchantTransactionID"]).last
      if success
        order   = @payment.try(:order)
        update_payment_transaction(@payment) if @payment

        if order
          order.payments.last.update_attributes(state: payment_state) rescue nil
          order.shipments.last.try(:ready)
          payment_total = order.payments.completed.sum(:amount) rescue 0.0
          order.update_attributes state: :complete, completed_at: Time.zone.now, payment_total: payment_total
          order.finalize! if transaction_status
          session[:order_id] = nil
        end
      end
    end

    def collecting_for_xml
      @payment = Spree::Bca::Transaction.where(transaction_no: params[:transactionNo]).last
      @error = {status: "00", message: {id: "Sukses", en: "Success"}}
      if @payment.nil?
        source = @order.payments.last.source rescue nil
        @error = {status: "01", message: {id: "Transaksi Anda tidak dapat diproses.", en: "Your transaction cannot be processed"}}
        source.update_attributes(response_message: @error.to_json) if source
      elsif ["completed", "APPROVED"].include?(@payment.try(:status))
        @error = {status: "01", message: {id: "Transaksi Anda telah dibayar", en: "Your transaction has been paid"}}
        @payment.update_attributes(response_message: @error.to_json)
      elsif @payment.expired?
        @error = {status: "01", message: {id: "Transaksi Anda telah kedaluwarsa", en: "Your transaction has expired"}}
        @payment.update_attributes(response_message: @error.to_json)
        @payment.payments.last.invalidate! rescue nil
      elsif !@payment.valid_params?(params)
        @error = {status: "01", message: {id: "Transaksi Anda tidak dapat diproses.", en: "Your transaction cannot be processed"}}
        @payment.update_attributes(response_message: @error.to_json)
        @payment.payments.last.invalidate! rescue nil
      else
        if @payment
          @payment.update_attributes status: 'completed', response_message: @error.to_json
        end
        if @payment && @payment.order && @payment.order.payments.valid.any?
          @payment_method = @payment.order.payments.valid.first.payment_method.options
          @payment.update_attributes response: params.to_json, approval_code: params["approvalCode"].to_json
          @payment.order.payments.valid.first.update_attributes(state: "completed") rescue nil
          @payment.order.shipments.last.try(:ready)
          payment_total = @payment.order.payments.completed.sum(:amount) rescue 0.0
          @payment.order.update_attributes state: :complete, completed_at: Time.zone.now, payment_total: payment_total
          @payment.order.finalize!
          session[:order_id] = nil
        end
        
        if @payment && @payment.donation
          @payment.donation.update_attributes(status: "completed")
          @payment.update_attributes response: params.to_json, approval_code: params["approvalCode"].to_json         
        end

        if @payment && @payment.member
          @payment.member.update_attributes payment_status: 'paid', expired_at: Time.zone.now + 1.year
          @payment.member.send_member_confirmation
          @payment.update_attributes response: params.to_json, approval_code: params["approvalCode"].to_json         
        end
      end
      @options = params if @payment
    end

    def receives
      load_order_with_lock
    end

    private
    def load_order_with_lock
      @order = current_order(lock: true) 
      @order = Spree::Order.find_by_number(params[:order_id]) unless @order
      @order = Spree::Order.find_by_number(params["merchantTransactionID"]) unless @order
      #redirect_to spree.cart_path and return unless @order
    end

    def set_processing
      payment = @order.payments.last if @order
      payment_method = payment.payment_method.options rescue nil
      #redirect_to spree.cart_path
      return '' unless payment_method
      source = payment.source
      misc_fee = @order.shipments.last.cost.to_f
      @order.adjustments.each do |adjustment|
        misc_fee += adjustment.amount.to_f
      end
      amount = @order.total - misc_fee
      amount = "#{amount.to_f.round(2)}#{amount.to_f.to_s.split('.').last.length == 1 ? '0' : ''}"
      misc_fee = "#{misc_fee.to_f.round(2)}#{misc_fee.to_f.to_s.split('.').last.length == 1 ? '0' : ''}"
      keys = {
                post_url: payment_method[:post_url],
                klik_pay_code: payment_method[:klik_pay_code],
                transaction_url: payment_method[:transaction_url],
                clear_key: payment_method[:clear_key],
                pay_type: payment_method[:pay_type],
                tenor: payment_method[:tenor],
                transaction_no: source.transaction_no,
                merchant_id: payment_method[:merchant_id],
                transaction_date: Time.zone.now.strftime("%d/%m/%Y %H:%M:%S"),
                callback: "http://#{Spree::Config[:site_url]}/payments/klikpay_inquiry?order_id=#{@order.number}&f_form=sprint",
                descp: "#{Spree.t('payment')} #{@order.number}",
                total_amount: amount,
                misc_fee: misc_fee
              }
      @content  = Spree::Bca::Processing.new(keys)
      payment.update_attributes(:response_code => source.transaction_no) rescue nil
      sync_transaction
    end

    def set_processing_donation
      @donation = Spree::Donation.find_by_id(params[:donation_id])
      payment_method = @donation.payment_method.options
      keys = {
                post_url: payment_method[:post_url],
                klik_pay_code: payment_method[:klik_pay_code],
                transaction_url: payment_method[:transaction_url],
                clear_key: payment_method[:clear_key],
                pay_type: payment_method[:pay_type],
                tenor: payment_method[:tenor],
                transaction_no: @donation.identifier,
                merchant_id: payment_method[:merchant_id],
                transaction_date: Time.zone.now.strftime("%d/%m/%Y %H:%M:%S"),
                callback: "http://#{Spree::Config[:site_url]}/payments/klikpay_inquiry?donation_id=#{@donation.id}&f_form=sprint",
                descp: "#{Spree.t('donation')} from #{@donation.name}",
                total_amount: "#{@donation.total.to_f.round(2)}#{@donation.total.to_f.to_s.split('.').last.length == 1 ? '0' : ''}"
             }
      @content  = Spree::Bca::Processing.new(keys)
      sync_transaction
    end

    def set_processing_member
      @member = Spree::User.find_by_id(params[:member])
      payment_method = @member.payment_method.options
      transactions = @member.bca_transactions.last
      amount = Spree::User.packages[@member.package_type.to_sym][:price].to_f rescue 0.0
      amount = "#{amount.round(2)}#{amount.to_s.split('.').last.length == 1 ? '0' : ''}"
      keys = {
                post_url: payment_method[:post_url],
                klik_pay_code: payment_method[:klik_pay_code],
                transaction_url: payment_method[:transaction_url],
                clear_key: payment_method[:clear_key],
                pay_type: payment_method[:pay_type],
                tenor: payment_method[:tenor],
                transaction_no: transactions.try(:transaction_no),
                merchant_id: payment_method[:merchant_id],
                transaction_date: Time.zone.now.strftime("%d/%m/%Y %H:%M:%S"),
                callback: "http://#{Spree::Config[:site_url]}/payments/klikpay_inquiry?member=#{@member.try(:id)}&f_form=sprint",
                descp: "#{Spree.t('be_a_member')} package #{@member.package_type}",
                total_amount: "#{amount}"
             }
      @content  = Spree::Bca::Processing.new(keys)
      sync_transaction
    end

    def sync_transaction
      bca_transaction = Spree::Bca::Transaction.where(transaction_no: @content.transaction_no).last
      if bca_transaction && @content.clear_key.present?
        bca_transaction.transaction_date = @content.transaction_date
        bca_transaction.auth_key = @content.auth_key
        bca_transaction.currency = @content.currency
        bca_transaction.total_amount = @content.total_amount
        bca_transaction.signature = @content.signature
        #set transaction expired time
        bca_transaction.expired_at = Time.zone.now + 3.minutes if bca_transaction.expired_at.nil?
        if bca_transaction.response_message.nil?
          if params[:f_form] == 'sprint'
            error = {status: "01", message: {id: "", en: ""}}
            bca_transaction.response_message = error.to_json
          end

          #if bca_transaction.expired_at and bca_transaction.expired_at < Time.zone.now && bca_transaction.response_message.nil?
          #  error = {status: "01", message: {id: "Transaksi Anda telah kedaluwarsa.", en: "Your transaction has expired"}}
          #  bca_transaction.response_message = error.to_json
          #  bca_transaction.payments.last.invalidate! rescue nil
          #end
        end
        bca_transaction.save
      end
    end
  end
end