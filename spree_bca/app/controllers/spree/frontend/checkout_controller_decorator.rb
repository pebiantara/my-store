module Spree
  CheckoutController.class_eval do   
    def update
      selected_method = params[:order][:payments_attributes].first[:payment_method_id] rescue nil
      @error = false
      if selected_method && params[:payment_source].present?
        payment_source = params[:payment_source][selected_method]
        payment_method = Spree::PaymentMethod.find(selected_method) rescue nil
        if payment_method && payment_method.is_a?(Spree::Bca::KlikPay)
          @error = true unless payment_source[:terms].present?
          if payment_source[:receive_email].present?
            #this is will use API from BCA to subscribe email
          end
          payment_source.delete_if{|x, v| [:receive_email, :terms].include?(x)} if payment_source
        end
      end

      if @error
        flash.now[:error] = Spree.t('terms_and_condition') + ', ' + Spree.t('should_be_checked')
        render :edit and return
      end 
       
      if @order.update_from_params(params, permitted_checkout_attributes)
        persist_user_address
        unless @order.next
          flash[:error] = @order.errors.full_messages.join("\n")
          redirect_to checkout_state_path(@order.state) and return
        end
        
        if @order.completed?
          session[:order_id] = nil
          flash.notice = Spree.t(:order_processed_successfully)
          flash[:commerce_tracking] = "nothing special"
          redirect_to completion_route
        else
          if params[:state].eql?('payment') && @order.payment_use_klikpay_or_veritrans?
            payment_method = @order.payments.valid.last.payment_method.options
            redirect_to "/payments/#{payment_method[:inquiry_path]}?order_id=#{@order.number}" and return
          end
          redirect_to checkout_state_path(@order.state)
        end
      else
        render :edit
      end
    end
  end
end