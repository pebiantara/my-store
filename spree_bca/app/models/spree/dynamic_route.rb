module Spree
  class DynamicRoute
    attr_accessor :routes, :inquiry_paths, :return_paths

    def initialize
      @routes = Spree::Core::Engine.routes.routes
    end

    def load_routes(model_klass)
      @inquiry_paths, @return_paths = [], []
      model_klass.where(type: [Spree::Bca::CreditCard, Spree::Bca::KlikPay], active: true).each do |payment_method|
        @inquiry_paths << payment_method.options[:inquiry_path]
        @return_paths << payment_method.options[:return_path]    
      end
      
      [[@inquiry_paths, 'klikpay_inquiry'], [@return_paths, 'klikpay_return']].each do |paths|
        creating_route(paths.first, paths.last)
      end
    end

    def creating_route(arr_path, type_path)
      arr_path.uniq.each do |ret_path|
        next if has_routed?(ret_path)
        Spree::Core::Engine.routes.draw do
          post "/payments/#{ret_path}" => "bca/payments##{type_path}", as: "bca_#{type_path}_#{ret_path}_post"
          get "/payments/#{ret_path}" => "bca/payments##{type_path}", as: "bca_#{type_path}_#{ret_path}"
        end
      end if return_paths.uniq.any?
    end

    def has_routed?(path_name)
      paths = @routes.collect {|r| r.path.spec.to_s }
      paths.each do |path|
        return true if path.split("/").include?(path_name)
      end
      false
    end
  end
end