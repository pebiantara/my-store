module Spree
  PaymentMethod.class_eval do

    after_save :reload_routes, if: :is_a_klikpay_or_veritrans?
    
    def self.providers
      unless Rails.application.config.spree.payment_methods.include?(Spree::Bca::KlikPay)
        Rails.application.config.spree.payment_methods << Spree::Bca::KlikPay
      end
      unless Rails.application.config.spree.payment_methods.include?(Spree::Bca::CreditCard)
        Rails.application.config.spree.payment_methods << Spree::Bca::CreditCard
      end
      Rails.application.config.spree.payment_methods
    end

    def self.load_routes
      Spree::DynamicRoute.new.load_routes(self)
    end

    def reload_routes
      Spree::PaymentMethod.load_routes
      Multitenant::Application.routes_reloader.reload!
    end

    def is_a_klikpay_or_veritrans?
      self.is_a?(Spree::Bca::KlikPay) or self.is_a?(Spree::Bca::CreditCard)
    end
  end
end
