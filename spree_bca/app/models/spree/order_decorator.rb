module Spree
  Order.class_eval do
    # def confirmation_required?
    #   Spree::Config[:always_include_confirm_step] ||
    #   payments.valid.map(&:payment_method).compact.any?(&:payment_profiles_supported?) ||
    #   state == 'confirm' || payments.valid.map{|x| x.payment_method.class.to_s}.include?(Spree::Bca::KlikPay.to_s)
    # end

    def payment_use_klikpay_or_veritrans?
      payments.valid.map{|x| x.payment_method.class.to_s}.include?(Spree::Bca::KlikPay.to_s) || payments.valid.map{|x| x.payment_method.class.to_s}.include?(Spree::Bca::CreditCard.to_s)
    end
  end
end