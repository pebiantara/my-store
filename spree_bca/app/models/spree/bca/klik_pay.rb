module Spree
  module Bca
    class KlikPay < Spree::PaymentMethod
      validates :name, presence: true
      attr_accessor :post_url, :merchant_name, :klik_pay_code, :http_method, :payment_flag_url, :payment_inquiry_url,
                    :clear_key, :pay_type, :success, :message

      preference :post_url, :string, default: "http://simpg.sprintasia.net:8779/klikpay/webgw"
      preference :gateway_type, :string, default: "simulator"
      preference :merchant_name, :string, default: ""
      preference :klik_pay_code, :string, default: ""
      preference :clear_key, :string, default: ""
      preference :pay_type, :string, default: "01"
      preference :tenor, :string, default: ""
      preference :inquiry_path, :string, default: "klikpay_inquiry"
      preference :return_path, :string, default: "klikpay_return"

      #PAYTYPE = {:"01" => "Full Transaction", :"02" => "Installment Transaction", :"03" => "Combination"}
      def authorize(amount, source, options_data={})
        source.success = true
        source
      end

      def cancel(response_code)
        if response_code
          #process cancel payment
        end
      end

      def payment_source_class
        Spree::Bca::Transaction
      end

      def method_type
        'klikpay'
      end

      def success?
        success
      end

      def authorization
        nil
      end

      def purchase(amount, source, hash_data)
        source.success = false
        source
      end

      def payment_profiles_supported?
        true
      end

      def provider
        integration_options = options
        @provider ||= provider_class.new(integration_options)
      end

      def options
        options_hash = {}
        preferences.each { |key, value| options_hash[key.to_sym] = value }
        options_hash
      end

      def provider_class
        self.class
      end
    end
  end
end