class Spree::Bca::Formula
  attr_accessor :klik_pay_code, 
                :transaction_no, 
                :currency, 
                :transaction_date, 
                :key_id, 
                :auth_key, 
                :signature, 
                :total_amount,
                :pay_type,
                :callback,
                :descp,
                :misc_fee,
                :tenor,
                :post_url,
                :gateway_type,
                :clear_key,
                :merchant_transaction_id,
                :transaction_id,
                :site_id,
                :service_version,
                :transaction_url,
                :transaction_type
 
  CURRENCY_PAD    = "1"
  PAY_CODE_PAD    = "0"
  TRANSACTION_PAD = "A"
  DATE_PAD        = "C"
  KEY_PAD         = "E"
  
  PAY_CODE_LENGTH  = 10
  TRANSACTION_LENGTH = 18
  CURRENCY_LENGTH = 5
  DATE_LENGTH = 19
  KEY_LENGTH = 32


  def initialize(attributes = {})
    @klik_pay_code    = attributes[:klik_pay_code] ? attributes[:klik_pay_code] : "01UTANKA08" #"01UTANKA08"
    @transaction_no   = attributes[:transaction_no] ? attributes[:transaction_no] : "456"
    @currency         = attributes[:currency] ? attributes[:currency] : "IDR"
    @transaction_date = attributes[:transaction_date] ? attributes[:transaction_date] : "20/01/2010 01:01:01" #14/01/2014 01:01:01"
    @total_amount     = attributes[:total_amount] ? attributes[:total_amount] : "15000.00"
    @pay_type         = attributes[:pay_type] ? attributes[:pay_type] : "02"
    @callback         = attributes[:callback] ? attributes[:callback] : "http://salihara-store.stagingapps.net:3000/payments/klikpay_return"
    @descp            = attributes[:descp] ? attributes[:descp] : "Test payment bca"
    @misc_fee          = attributes[:misc_fee] ? attributes[:misc_fee] : "00"
    @tenor            = attributes[:tenor] ? attributes[:tenor] : "03"
    @post_url         = attributes[:post_url] ? attributes[:post_url] : "http://simpg.sprintasia.net:8779/klikpay/webgw"
    @gateway_type     = attributes[:gateway_type] ? attributes[:gateway_type] : "simulator"
    @clear_key        = attributes[:clear_key] ? attributes[:clear_key] : ""
    @transaction_id   = attributes[:transaction_id] ? attributes[:transaction_id] : ""
    @site_id          = attributes[:site_id] ? attributes[:site_id] : "salihara"
    @service_version  = attributes[:service_version] ? attributes[:service_version] : "2.1"
    @transaction_url  = attributes[:transaction_url] ? attributes[:transaction_url] : ""
    @merchant_transaction_id = attributes[:merchant_transaction_id] ? attributes[:merchant_transaction_id] : ''
    @transaction_id   = attributes[:transaction_id] ? attributes[:transaction_id] : ''
    @transaction_type   = attributes[:transaction_type] ? attributes[:transaction_type] : ''
  end

  def recreate_pay_code
    klik_pay_code_created = ''
    if @klik_pay_code.length < PAY_CODE_LENGTH
      (0..(PAY_CODE_LENGTH - @klik_pay_code.length - 1)).each do |x|
        klik_pay_code_created += PAY_CODE_PAD
      end
      klik_pay_code_created = @klik_pay_code.to_s + klik_pay_code_created
    else
      klik_pay_code_created = @klik_pay_code.to_s
    end
    klik_pay_code_created
  end

  def recreate_transaction
    transaction_created = ''
    if @transaction_no.to_s.length < TRANSACTION_LENGTH
      (0..(TRANSACTION_LENGTH - @transaction_no.to_s.length - 1)).each do |x|
        transaction_created += TRANSACTION_PAD
      end
      transaction_created = @transaction_no.to_s + transaction_created
    else
      transaction_created = @transaction_no.to_s
    end
    transaction_created
  end

  def recreate_currency
    currency_created = ''
    if @currency.length < CURRENCY_LENGTH
      (0..(CURRENCY_LENGTH - @currency.length - 1)).each do |x|
        currency_created += CURRENCY_PAD
      end
      currency_created = @currency.to_s + currency_created
    else
      currency_created = @currency.to_s
    end
    currency_created
  end

  def recreate_key_id
    key_id_created = ''
    if key_id.length < KEY_LENGTH
      (0..(KEY_LENGTH - key_id.length - 1)).each do |x|
        key_id_created = key_id.to_s.+(KEY_PAD)
      end
    else
      key_id_created = key_id
    end
    key_id_created
  end

  def recreate_transaction_date
    transaction_date_created = ''
    if @transaction_date.length < DATE_LENGTH
      (0..(DATE_LENGTH - @transaction_date.length - 1)).each do |x|
        transaction_date_created += DATE_PAD
      end
      transaction_date_created = @transaction_date.to_s + transaction_date_created
    else
      transaction_date_created = @transaction_date.to_s
    end
    transaction_date_created
  end

  def auth_key
    encrypt_data
  end

  def decrypt_key_id
    fk = []
    (0..key_id.length/2).each do |x|
      fk << key_id[(x*2)..((x*2)+1)].hex.chr unless key_id[(x*2)..((x*2)+1)].blank?
    end
    (0..7).each do |x|
      fk << fk[x]
    end
    fk.join
  end

  def decrypt_auth_md5
    fstr = []
    (0..auth_convert_md5.length/2).each do |x|
      fstr << auth_convert_md5[(x*2)..((x*2)+1)].hex.chr unless auth_convert_md5[(x*2)..((x*2)+1)].blank?
    end
    fstr.join
  end

  def encrypt_data
    iv = "".ljust(8, "\0")
    encrypted = encrypt_3des(decrypt_key_id, iv, decrypt_auth_md5)
    out = ''
    (0..15).each do |x|
      next if encrypted[x].nil?
      v = encrypted[x].ord.to_s(16)
      output = v.length == 1 ? "0"+v : v
      out += output
    end
    out.upcase
  end

  def encrypt_3des(key,iv,data)
    cipher     = OpenSSL::Cipher::Cipher.new("des-ede3")   
    cipher.encrypt
    cipher.key = key
    cipher.iv  = iv
    ciphertext = cipher.update(data)
    ciphertext << cipher.final
    ciphertext
  end

  # def decrypt_data
  #   cipher     = OpenSSL::Cipher::Cipher.new("AES-128-CBC")   
  #   cipher.decrypt
  #   cipher.key = key_id
  #   #cipher.pkcs5_keyivgen(key_id)
  #   plaintext = cipher.update(data)
  #   plaintext << cipher.final
  #   plaintext
  # end
  
  def auth_convert_md5
    Digest::MD5.hexdigest(auth_data_one).upcase
  end

  def auth_data_one
    "#{recreate_pay_code}#{recreate_transaction}#{recreate_currency}#{recreate_transaction_date}#{recreate_key_id}"
  end

  def signature
    hash_one = calculate(first_hash.to_s)
    hash_two = calculate(second_hash.to_s)
    (hash_one + hash_two).abs
  end

  def first_hash
    @klik_pay_code + @transaction_no + @currency + key_id
  end

  def second_hash
    @transaction_date.split(" ").first.gsub("/", "").to_i + @total_amount.to_i
  end

  def key_id
    keys = ""
    bytes = clear_key.bytes
    bytes.each{|x| keys += x.to_s(16)}
    keys.upcase
  end

  def calculate(value)
    minVal = -2147483648;
    maxVal = 2147483647;
    hash   = 0;
    #for i in 0..(value.length - 1)
    value.each_byte do |val|
      hash = ( hash * 31 ) + val
      
      while hash > maxVal do
        hash = hash + minVal - maxVal - 1
      end

      while  hash < minVal do
        hash = hash + maxVal - minVal + 1
      end
    end
    hash
  end
end