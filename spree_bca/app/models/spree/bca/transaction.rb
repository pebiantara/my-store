module Spree
  module Bca
    class Transaction < ActiveRecord::Base
      self.table_name = "spree_bca_payments"
      include TenantScoped
      belongs_to :order
      belongs_to :donation
      belongs_to :member, class_name: Spree::User, foreign_key: :member_id
      attr_accessor :payment_method_id, :user_id
      has_many :payments, as: :source

      def self.statuses
        {
          "00" => {id: "Sukses", en: "Success"},
          "01" => {id: "Gagal", en: "Failed"}
        }
      end

      def valid_params?(params)
        date = transaction_date.strftime("%d/%m/%Y %H:%M:%S").eql?(params["transactionDate"]) rescue false
        diff_auth_key = auth_key.eql?(params["authKey"])
        diff_amount = total_amount.eql?(params["totalAmount"])
        diff_currency = currency.eql?(params["currency"])

        date && diff_auth_key && diff_amount && diff_currency
      end

      def expired?
        !expired_at.nil? and self.expired_at < Time.now
      end

      def response_message
        return super unless super
        JSON.parse(super) rescue super
      end

      def response
        return super unless super
        JSON.parse(super) rescue super
      end
    end
  end
end