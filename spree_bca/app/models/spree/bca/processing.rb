class Spree::Bca::Processing < Spree::Bca::Formula

  def starting
    url = post_url
    params = {
              klikPayCode: klik_pay_code,
              transactionNo: transaction_no,
              totalAmount: total_amount,
              currency: currency,
              payType: pay_type,
              callback: callback,
              transactionDate: transaction_date,
              descp: descp,
              miscFee: misc_fee,
              signature: signature
             }

    if gateway_type != "simulator"
      params = params.merge(auth_key: auth_key)
    end

    requesting(url, params)
    #JSON.parse(results) rescue nil
  end

  def requesting(url, params)
    begin
      results = RestClient.post(url, params){ |response, request, result, &block|
        if [301, 302, 307].include? response.code
          puts request.inspect
          puts result.inspect
          response.follow_redirection(request, result, &block)
        else
          puts request.inspect
          puts result.inspect
          response.return!(request, result, &block)
        end
      }
    rescue
      text = Spree.t('flash.message.errors.gateway')
      raise Spree::Core::GatewayError.new(text)
    end
  end

  def authorize!
    url = transaction_url
    params = {
               siteID: site_id, 
               serviceVersion: service_version,
               merchantTransactionID: merchant_transaction_id, 
               transactionType: transaction_type, 
               currency: currency,
               amount: total_amount
             }
    requesting(url, params)
  end
end