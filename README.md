### README

This README would normally document whatever steps are necessary to get the application up and running.

Things you may want to cover:
This is sources of multiple store based of Spree(spreecommerce.com)

What the customized
1. Spree API
2. Spree Core
3. Spree Backend
4. Spree Frontend

#### Spree Version 2.2.2
Spree extention
spree gateway
spree auth devise
spree multitenant
spree i18n
spree wishlist
spree email to friend
spree editor

#owned extension
spree_customized path = 'spree_customized'
spree_bca path = 'spree_bca'

#### Ruby version
ruby 2.1.2p95
#### System dependencies
Rails 4.0.5 included(all system of rails like rake, bundle and etc)

#### Configuration
Creating new store
bundle exec rake spree_multi_tenant:create_tenant domain=name of domain code=code store

Generating menu
rake menu:generate

Recreating qrcode
rake qrcode:recreate

#### What need to complete?
1. wishlist [done]
2. jne shipment [done]
3. bca payment(Spree::Bca::KlikPay) & veritrans(Spree::Bca::CreditCard) [done]
4. configuration menu [done]
5. product discount [done]
6. product qrcode [done]
7. slider management(product image promotion) [done]
8. livechat [research]

#### Infrastructure
+spreecommerce
  +app
    +controllers
    +helpers
    +models
    +mailers
    +views
    +overrides
  +bin
  +config
  +db
  +lib
  +log
  +public
  +spree(all controller backend/frontend from spree source)
  +spree_bca(extension)
  +spree_customized(extension)
    +app
    +templates
      +andromeda
  +test
  +vendor

#### Rule of work
if you will work for new store please to create new branch give the name same as code of store(for example salihara)
you will work in the spree_customized directory
if you need to made a custom source and the changes is only for one store please write your code in the folder tenants/(your store), but if you thing what you will change can use for all store you can write code in the app directory in spree_customized.

#### How to deploy
you can made request to your cordinator or project manager for deploy to staging or production, but please to make sure all code is good and not have any affect for another store.

#### Services
currenty in this source not have any services, but maybe later we will add some service like a node for live chat, or some cron job if needed
LiveChat will use socket.io, redis, and node js, command for running this service will available soon.


#### Note
Remember when you will create some global feature please to discuss before you do.

#### Author
pebiantara
pebiantara@gmail.com
skype: pebiantara
github: 

you can send me email if you have any question.


Thank You